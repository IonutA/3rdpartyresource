////////////////////////////////////////////////////////////////////////// 
///
/// \file jmpcan.h
///
/// Header file defining macro values and data structures needed to be able
/// to communicate with the PCAN device driver.
///
/// \author cm, Janz Automationssysteme AG
///
/// \date 2004/V1.0 of the device driver
///
/// \see pcan_register_callback
///
///////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////// 


#ifndef __JMPCAN_H__
#define __JMPCAN_H__


///////////////////////////////////////////////////
// SJA1000 register and bit definitions *
///////////////////////////////////////////////////
 
/* Values of output control register */
#define OCR_PUSHPULL	0xfa	/* push/pull */

/*
 * SJA1000 in BasicCAN Mode
 */
#define BCAN_CR		 0	/* control register */
#define BCAN_CMR	 1	/* command register */
#define BCAN_SR		 2	/* status register */
#define BCAN_IR		 3	/* interrupt register */
#define BCAN_AC		 4	/* acceptance code register */
#define BCAN_AM		 5	/* acceptance mask register */
#define BCAN_BT0	 6	/* bus timing register 0 */
#define BCAN_BT1	 7	/* bus timing register 1 */
#define BCAN_OCR	 8	/* output control register */
#define BCAN_TDESC1	10	/* first descriptor transmit buffer */
#define BCAN_TDESC2	11	/* second descriptor transmit buffer */
#define BCAN_TDATA	12	/* start data transmit buffer */
#define BCAN_RDESC1	20	/* first descriptor receive buffer */
#define BCAN_RDESC2	21	/* second descriptor receive buffer */
#define BCAN_RDATA	22	/* start data receive buffer */
#define BCAN_CDR	31	/* clock divider */


/* Admissible values in clock divider register */
#define CDR_16MHZ	0x07	/* 16 MHz output */
#define CDR_8MHZ	0x00	/* 8 MHz output (division by 2) */
#define CDR_4MHZ	0x01	/* 4 MHz output (division by 4) */
#define CDR_2MHZ	0x03	/* 2 MHz output (division by 8) */

/* Control Register Bits */
#define BCAN_CR_OIE		0x10	/* Overrun Interrupt Enable */
#define BCAN_CR_EIE		0x08	/* Error Interrupt Enable */
#define BCAN_CR_TIE		0x04	/* Transmit Interrupt Enable */
#define BCAN_CR_RIE		0x02	/* Receive Interrupt Enable */
#define BCAN_CR_RR 		0x01	/* Reset Request */

/* Command Register Bits */
#define BCAN_CMR_GTS	0x10	/* Goto Sleep */
#define BCAN_CMR_COS	0x08	/* Clear Overrun Status */
#define BCAN_CMR_RRB	0x04	/* Release Receive Buffer */
#define BCAN_CMR_AT		0x02	/* Abort Transmission */
#define BCAN_CMR_TR		0x01	/* Transmission Request */

/* Status Register Bits */
#define BCAN_SR_BS		0x80	/* Bus Status */
#define BCAN_SR_ES		0x40	/* Error Status */
#define BCAN_SR_TS		0x20	/* Transmit Status */
#define BCAN_SR_RS		0x10	/* Receive Status */
#define BCAN_SR_TCS		0x08	/* Transmission Complete Status */
#define BCAN_SR_TBS		0x04	/* Transmit Buffer Status */
#define BCAN_SR_DO		0x02	/* Data Overrun */
#define BCAN_SR_RBS		0x01	/* Receive Buffer Status */

/* Interrupt Register Bits */
#define BCAN_IR_WUI		0x10	/* Wake-Up Interrupt */
#define BCAN_IR_OI		0x08	/* Overrun Interrupt */
#define BCAN_IR_EI		0x04	/* Error Interrupt */
#define BCAN_IR_TI		0x02	/* Transmit Interrupt */
#define BCAN_IR_RI		0x01	/* Receive Interrupt */


/*
 * SJA1000 in PeliCAN mode
 */

/* PeliCAN mode address allocation */
#define PCAN_MODR        0      /* Mode register (rw) */
#define PCAN_CMR         1		/* Command register (wo) */
#define PCAN_SR          2      /* Status register (ro) */
#define PCAN_IR          3		/* Interrupt register (ro) */
#define PCAN_IER         4      /* Interrupt enable register (rw) */
#define PCAN_BTR0        6		/* Bus timing register 0 (ro, rw) */
#define PCAN_BTR1        7		/* Bus timing register 1 (ro, rw) */
#define PCAN_OCR         8		/* Output control register 1 (ro, rw) */
#define PCAN_TESTR       9		/* Test register */
#define PCAN_ALCR       11		/* Arbitration lost capture reg (ro) */
#define PCAN_ECCR       12      /* Error code capture register (ro) */
#define PCAN_EWLR       13      /* Error warning limit register (ro, rw) */
#define PCAN_RXERR      14		/* Rx error counter register (ro, rw) */
#define PCAN_TXERR      15      /* Tx error counter register (ro, rw) */
#define PCAN_ACR0		16		/* acceptance code register 0 (-, rw) */
#define PCAN_ACR1		17		/* acceptance code register 1 (-, rw) */
#define PCAN_ACR2		18		/* acceptance code register 2 (-, rw) */
#define PCAN_ACR3		19		/* acceptance code register 3 (-, rw) */
#define PCAN_AMR0		20		/* acceptance mask register 0 (-, rw) */
#define PCAN_AMR1		21		/* acceptance mask register 1 (-, rw) */
#define PCAN_AMR2		22		/* acceptance mask register 2 (-, rw) */
#define PCAN_AMR3		23		/* acceptance mask register 3 (-, rw) */
#define PCAN_RXFI       16      /* Rx Frame info   SFF, EFF (ro, -) */
#define PCAN_RXID1      17      /* Rx Identifier 1 SFF, EFF (ro, -) */
#define PCAN_RXID2      18      /* Rx Identifier 2 SFF, EFF (ro, -) */
#define PCAN_RXID3      19      /* Rx Identifier 3      EFF (ro, -) */
#define PCAN_RXID4      20      /* Rx Identifier 4      EFF (ro, -) */
#define PCAN_RXSFFD     19      /* Rx standard frame data   (ro, -) */
#define PCAN_RXEFFD     21      /* Rx extended frame data   (ro, -) */
#define PCAN_TXFI       16      /* Tx Frame info   SFF, EFF (wo, -) */
#define PCAN_TXID1      17      /* Tx Identifier 1 SFF, EFF (wo, -) */
#define PCAN_TXID2      18      /* Tx Identifier 2 SFF, EFF (wo, -) */
#define PCAN_TXID3      19      /* Tx Identifier 3      EFF (wo, -) */
#define PCAN_TXID4      20      /* Tx Identifier 4      EFF (wo, -) */
#define PCAN_TXSFFD     19      /* Tx standard frame data   (wo, -) */
#define PCAN_TXEFFD     21      /* Tx extended frame data   (wo, -) */
#define PCAN_RXMCR      29		/* Rx message counter register (ro) */
#define PCAN_RXBSAR     30		/* Rx buffer start address register (ro, rw) */
#define PCAN_CDR		31		/* Clock divider register ('rw', rw)*/

#define PCAN_RXFI_RAM	96		/* RAM mirror of RXFI */
#define PCAN_TXFI_RAM	96		/* RAM mirror of TXFI */
#define PCAN_TXID1_RAM  97      /* RAM mirror Tx Identifier 1 SFF, EFF  */
#define PCAN_TXID2_RAM  98      /* RAM mirror Tx Identifier 2 SFF, EFF  */
#define PCAN_TXID3_RAM  99      /* RAM mirror Tx Identifier 3      EFF  */
#define PCAN_TXID4_RAM  100     /* RAM mirror Tx Identifier 4      EFF  */
#define PCAN_TXSFFD_RAM 99      /* RAM mirror Tx standard frame data    */
#define PCAN_TXEFFD_RAM 101     /* RAM mirror Tx extended frame data    */


#define PCAN_TERM		256		/* register offset address for setting/resetting 
								 * CAN bus termination 
								 */
#define PCAN_LED		258		/* register offset address for setting LED status */


/* Mode Register Bits */
#define PCAN_MODR_SM    (1<<4)	/* Sleep mode */
#define PCAN_MODR_AFM   (1<<3)	/* Acceptance filter mode */
#define PCAN_MODR_STM   (1<<2)	/* Self test mode */
#define PCAN_MODR_LOM   (1<<1)	/* Listen only mode */
#define PCAN_MODR_RM    (1<<0)	/* Reset mode */

/* Command Register Bits */
#define PCAN_CMR_SRR	(1<<4)	/* Self reception request */
#define PCAN_CMR_CDO	(1<<3)	/* Clear data overrun */
#define PCAN_CMR_RRB	(1<<2)	/* Release receive buffer */
#define PCAN_CMR_AT		(1<<1)	/* Abort transmission */
#define PCAN_CMR_TR		(1<<0)	/* Transmission request */

/* Status Register Bits */
#define PCAN_SR_BS		(1<<7)	/* Bus status */
#define PCAN_SR_ES		(1<<6)	/* Error status */
#define PCAN_SR_TS		(1<<5)	/* Transmit status */
#define PCAN_SR_RS		(1<<4)	/* Receive status */
#define PCAN_SR_TCS		(1<<3)	/* Transmission complete status */
#define PCAN_SR_TBS		(1<<2)	/* Transmit buffer status */
#define PCAN_SR_DOS		(1<<1)	/* Data overrun status */
#define PCAN_SR_RBS		(1<<0)	/* Receive buffer status */

/* Interrupt Register Bits */
#define PCAN_IR_BEI		(1<<7)	/* Bus-error interrupt */
#define PCAN_IR_ALI		(1<<6)	/* Arbitration lost interrupt */
#define PCAN_IR_EPI		(1<<5)	/* Error-passive interrupt */
#define PCAN_IR_WUI		(1<<4)	/* Wake-up interrupt */
#define PCAN_IR_DOI		(1<<3)	/* Data-overrun interrupt */
#define PCAN_IR_EI		(1<<2)	/* Error interrupt */
#define PCAN_IR_TI		(1<<1)	/* Transmit interrupt */
#define PCAN_IR_RI		(1<<0)	/* Receive interrupt */

/* Interrupt enable register bits */
#define PCAN_IER_BEIE	(1<<7)	/* Bus-eror interrupt enable */
#define PCAN_IER_ALIE	(1<<6)	/* Arbitration lost interrupt enable */
#define PCAN_IER_EPIE	(1<<5)	/* Error-passive interrupt enable */
#define PCAN_IER_WUIE	(1<<4)	/* Wake-up interrupt enable */
#define PCAN_IER_DOIE	(1<<3)	/* Data-overrun interrupt enable */
#define PCAN_IER_EIE	(1<<2)	/* Error warning interrupt enable */
#define PCAN_IER_TIE	(1<<1)	/* Transmit interrupt enable */
#define PCAN_IER_RIE	(1<<0)	/* Receive interrupt enable */

/* Clock divider register bits */
#define PCAN_CDR_PCAN	(1<<7)	/* Enable PCAN mode */
#define PCAN_CDR_CBP	(1<<6)	/* Enable Rx input comparator bypass */
#define PCAN_CDR_RXINTEN (1<<5)	/* Enable RXINT output at TX1 */
#define PCAN_CDR_CLKOFF	(1<<3)	/* Disable clock output */ 
    
#ifndef BTR_1MB
/* Admissible bus timing values (BTR1=msb, BTR0=lsb) */
#define BTR_1MB		0x1400	/* 1 MBaud */	 /* is opt setting! */
#define BTR_800KB	0x2500	/* 800 KBaud */
#define BTR_500KB	0x1c00	/* 500 KBaud */
#define BTR_250KB	0x1c01	/* 250 KBaud */
#define BTR_125KB	0x1c03	/* 125 KBaud */
#define BTR_100KB	0x34c7	/* 100 KBaud */
#define BTR_62_5KB	0xbac7	/*  62.5 KBaud */
#define BTR_50KB	0x34cf	/*  50 KBaud */
#define BTR_20KB	0x7fcf	/*  20 KBaud */
#define BTR_10KB	0x7fdf	/*  10 KBaud */
#endif

/* Admissible values in clock divider register */
#ifdef OSC_16MHZ		/* if using a 16 MHz oscillator */
#define CDR_16MHZ	0x07	/* 16 MHz output */
#define CDR_8MHZ	0x00	/* 8 MHz output (division by 2) */
#define CDR_4MHZ	0x01	/* 4 MHz output (division by 4) */
#define CDR_2MHZ	0x03	/* 2 MHz output (division by 8) */
#endif


/***** IOCTL commands */

#define PCAN_SETBTR          0     /* Configure baud rate */
#define PCAN_BUSON           1     /* Switch buson */
#define PCAN_BUSOFF          2     /* Switch busoff */
#define PCAN_INQUIRY         3     /* Get status information */
#define	PCAN_INIT_POOL	     4	   /* install message pool   	*/
#define PCAN_CONF_BERR       5     /* bus-error detection */
#define PCAN_CONF_EWL        6     /* error warning limit */
#define PCAN_CONF_LOM        7     /* listen only mode */
#define PCAN_CONF_STM        8     /* self test mode */
#define PCAN_CONF_SNIFF      9     /* control sniffing */
#define PCAN_CONF_AFIL      10     /* control acceptance filter */
#define PCAN_CONF_AFIL64    11     /* control acceptance filter (64bit) */
#define PCAN_CONF_AFILOPT   12     /* control acceptance filter options */
#define PCAN_CONF_AFILLOCK  13     /* lock acceptance filter */
#define PCAN_CONF_ACM       14     /* configure hardware accpetance filter */
#define PCAN_SET_SYNC_MSG   15     /* set message send on sync */
#define PCAN_SET_SYNC_PERIOD 16    /* set frequency of sync */
#define PCAN_CONF_TERM      17     /* control termination */
#define PCAN_SET_ERRCNT     18     /* write error counters */
#define PCAN_SET_REG        19     /* write a register */
#define PCAN_GET_REG        20     /* read a register */
#define PCAN_TEST           21     /* For testing */
#define PCAN_TESTDUMP       22     /* For testing: dump controller */

#define PCAN_SETLED			23     /* turns LEDs on the device on and off */

#define PCAN_RESET_DEVICE	48	   /* initialize the hardware device */
#define PCAN_CALL_BOOTLD	49	   /* call bootloader */

/* Bits in PCAN_CONF_SNIFF argument */
#define PCAN_CONF_SNIFF_RX    1
#define PCAN_CONF_SNIFF_TX    2


/* Definitions for INQUIRY service */
#define PCAN_INQUIRY_STATUS          0
#define PCAN_INQUIRY_BUSLOAD         1
#define PCAN_INQUIRY_DRIVERVERSION   2
#define PCAN_INQUIRY_POOLSTAT        3
#define PCAN_INQUIRY_CTRL_SJA1000    2
    
#define PCAN_INQUIRY_STATUS_BUFLEN   8
#define PCAN_INQUIRY_BUSLOAD_BUFLEN  (8*4)
#define PCAN_INQUIRY_POOLSTAT_BUFLEN (3*4)
    
#define PCAN_INQUIRY_CTRL_82C200   1
#define PCAN_INQUIRY_CTRL_SJA1000  2


/* Miscellaneous definitions */
    
#define PCAN_MASK_REJECT   0
#define PCAN_MASK_ACCEPT   1
    
#define PCAN_AFIL_LOCK     1
#define PCAN_AFIL_UNLOCK   0
#define PCAN_AFIL_LOCK_REJECT       1  /* Accept all frames when f. is locked */
#define PCAN_AFIL_LOCK_ACCEPT       0  /* Reject all frames when f. is locked */
#define PCAN_AFIL_LOCK_DISABLE_INT  2  /* Disable Rx-Int during filter lock */

    
/***** Message definitions read()/write() interface */

/* Values for <spec> field in pcanXxxMsg */
#define PCAN_SPEC_RSVD	      0
#define PCAN_SPEC_CAN         1   /* send/receive CAN messages */
#define PCAN_SPEC_EVT         2   /* receive CAN events */
#define PCAN_SPEC_TXSNIFF     3   /* receive sniffed Tx */
#define PCAN_SPEC_RXSNIFF     4   /* future: receive sniffed Rx */
#define PCAN_SPEC_SYNCIND     5   /* sync has been triggered */
#define PCAN_SPEC_IOCTL		  16  /* Control message (aka IOctl) */
#define PCAN_SPEC_ERRMSG	  32  /* firmware error messages */
#define PCAN_SPEC_FWUPDATE	  127 /* firmware update message */


/* Values for <finfo> field in pcanCanMsg_t */
#define PCAN_FINFO_FF	(1<<7)	/* Frame format bit */
#define PCAN_FINFO_EFF	(1<<7)	/* Extended frame indication */
#define PCAN_FINFO_RTR	(1<<6)	/* RTR frame bit */
#define PCAN_FINFO_DLC_MASK (0x0f) /* Data length code mask */

/* Values for the <flags> field in pcanCanMsg_t */
#define PCAN_FL_ECHO	 (1<<4)	/* Request echo (=self receipt) */
#define PCAN_FL_SNGL	 (1<<1)	/* Request single shot transmission */
#define PCAN_FL_QUEUE(X) ((X)<<5) /* Request transmission queue */
#define PCAN_FL_GETQUEUE(X) (((X)>>5)&0x7) /* Get requested transmission queue */
   
/* Values for <type> field in pcanEvtMsg_t */
#define PCAN_EVT_RSVD       0x00
#define PCAN_EVT_ERROR      0x01    /* bus-off, bus-on, reached ewl, ... */
#define PCAN_EVT_OVERRUN    0x02    /* lost CANbus frames */
#define PCAN_EVT_LOST_IRQ   0x04    /* For future */
#define PCAN_EVT_QUEUE_FULL 0x08    /* For future */
#define PCAN_EVT_BERR       0x10    /* bus-error feature of SJA1000  */


#define MAX_NUM_TX_QUEUES	0x04	// static number of Tx queues automatically created 
									// in the driver
#define MAX_LEN_TX_QUEUES	0xFF	// length of a Tx queue (unit = number of messages)


//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//
// DATA TYPE DEFINITIONS
//

////////////////////////////////////////////////////////////////////////// 
///
/// Generic message buffer for PCAN read/write
///
/// All data read and write through a PCAN device will be transported
/// using this generic message format: A specifier and 15 data bytes.
/// The data can only be understood by knowledge of the specifier.
/// 
/// You can use pcan_void_msg when you call read() and don't know
/// what message type you will read next (It's just for better
/// readability).
///
/// \note
/// This type of message structure is not used for ioctl, which has completely
/// different data passing format.

typedef struct pcan_void_msg 
{
	/// Message type specifier
    unsigned char spec;
	/// Generic data
    unsigned char data[31];
} pcanVoidMsg_t;


////////////////////////////////////////////////////////////////////////// 
///
/// Message for transporting CAN frames.
///
/// This is the format to send/receive CAN frames from a PCAN device.
/// It is used when \a spec is one of
/// PCAN_SPEC_CAN, PCAN_SPEC_TXSNIFF or PCAN_SPEC_RXSNIFF
///
/// The \a finfo element has the following bit fields:
///
/// ERxxLLLL
///
/// - E: Extended frame flag (when 1)
/// - R: RTR flag (when 1)
/// - L: Frame length info
///
/// The \a flags element has some bit flags that can be used to control
/// transmission requests:
///
/// QQQRxxSx
/// - S: Single shot transmission (no-retry on fault)
/// - R: Self reception (echo)
/// - Q: Queue where to put transmitt requests (0..n). Number of
/// queues available depends on driver compile time options (default
/// is 4). Queue 0 has least priority (and will have ID sorting in
/// future), Queue 1 hast hightest priotiry, and Queue 1..n have
/// falling priority.
///
/// The timestamp is valid for received messages. It is measured in
/// mircoseconds, and wraps around after 1 hour. As the timestamp
/// is captured in software during the Rx interrupt service
/// routine, the accuray will suffer from interrupt latency.
///
////////////////////////////////////////////////////////////////////////// 

typedef struct pcan_can_msg
{
	/// Mesaage type specifier
    unsigned char    spec;
	/// Frame info
    unsigned char    finfo;
	/// Device flags
    unsigned char    flags;
	/// Unused byte
    unsigned char    unused;
	/// CAN message identifier
    unsigned int     id;
	/// CAN message data
    unsigned char    data[8];
	/// Timestamp
    unsigned int     timestamp;
	/// Reserverd bytes
    unsigned char    rsv[4];
	/// Application handle (low part)
	ULONGLONG handle;
} pcanCanMsg_t;

////////////////////////////////////////////////////////////////////////// 
///
/// Mssage for receiving events
///
/// This is the format in which events (overrun events, bus errors etc.)
/// are received  from a PCAN device. It is used when \a spec is
/// PCAN_SPEC_EVT.
/// 
/// The event type \a type specifies the format of the \a data field.
/// The following events are generated by the SAJ1000:
///
/// - PCAN_EVT_OVERRUN:
///   Generated when Rx interrupts are lost in the SJA1000. This occurs
///   when the ISR cannot handle incoming messages fast enough and
///   a CAN frame arrives when the Rx fifo is full.
///
/// - PCAN_EVT_ERROR:
///   This occurs when either of the Rx or Tx error counters reach
///   the (configurable) error passive level or when the controller
///   reaches error passive state.
///   This type of error carries additional status information:
///
///    -# d[0] = PCAN_INQUIRY_CTRL_SJA1000 = 2
///    -# d[1] = Mode register (MODR)
///    -# d[2] = Status register (SR)
///    -# d[3] = Rx error counter (RXERR)
///    -# d[4] = Tx error counter (TXERR)
///
/// - PCAN_EVT_BERR:
///   This kind of error is reported for each physical CAN error that
///   is detected by the SAJ1000, while the bus error quota is greater
///   than zero. The quota is counted down for each reported bus
///   error.
///   This type of error carries additional status information:
///
///    -# d[0] = PCAN_INQUIRY_CTRL_SJA1000 = 2
///    -# d[1] = Error code capture Register (ECCR)
///    -# d[2] = Status register (SR)
///    -# d[3] = Rx error counter (RXERR)
///    -# d[4] = Tx error counter (TXERR)
///    -# d[5] = remaining quota
///
/// \see
///    pcan_config_berr()
///
////////////////////////////////////////////////////////////////////////// 

typedef struct pcan_evt_msg 
{
	/// Message type specifier
    unsigned char   spec;
	/// Error type
    unsigned char   type;
	/// Additional error type specific data 
    unsigned char   data[30];
} pcanEvtMsg_t;


////////////////////////////////////////////////////////////////////////// 
///
/// Message for receiving events.
///
/// This is the format in which control commands are sent to the device.
/// 
/// The command specifier is saved in control byte 0. If needed control
/// byte 2 can be used to save a sub-specifier.
///
////////////////////////////////////////////////////////////////////////// 

typedef struct pcan_ctrl_msg 
{
	/// Message type specifier
    unsigned char   spec;
	/// Error type
    unsigned char   ctrl[2];
	/// Additional error type specific data
    unsigned char   data[29];
} pcanCtrlMsg_t;


/// \struct _mpcan_ioctl_header
/// Contains header information which are common to all data packages
/// exchanged between host application and driver
typedef struct _mpcan_ioctl_header
{
	/// \var rc
	/// This variable contains a result code which signals error or success
	/// of a driver function. It is used to transport more detailed information
	/// about error situations in a device driver. So, the driver not only
	/// signals, that something went wrong or not, it is possible to say, what
	/// error occurred returning an corresponding error code. As return values
	/// the macros defined in file jmpcan_err.h are used.
	long rc;
} mpcan_ioctl_header;
/// \typedef mpcan_hw_access
/// Data type used to tranfer data mainly from driver to the host application
/// which is common to all data transfer packages.

/// \struct _mpcan_hw_access
/// The data structure is used to define the type mpcan_hw_access.
typedef struct _mpcan_hw_access
{
	/// \var header
	/// Contains header information which are common to all data packages
	/// exchanged between host application and driver
	mpcan_ioctl_header header;
	/// \var cmdCode
	/// Code for a command to be executed by the driver
	unsigned char cmdCode;
} mpcan_hw_access;
/// \typedef mpcan_hw_access
/// Data type used to tranfer data for simple hardware access functions without
/// data to be returned

/// \struct _mpcan_ext_hw_access
/// The data structure was defined for hardware access function which need
/// additional data to send or receive except the four bytes already define in
/// the base structure mpcan_hw_access. For this the data structure contains structure
/// mpcan_hw_access and an additional 16 byte data buffer which will be used to transfer
/// additional data between driver and library.
typedef struct _mpcan_ext_hw_access
{
	/// \var header
	/// Contains header information which are common to all data packages
	/// exchanged between host application and driver
	mpcan_ioctl_header header;
	/// \var data
	/// Buffer for additional data bytes.
	/// The contents will vary from function to function. It will always contain the data
	/// bytes described for the user commands in the hardware manual of the LM628.
	unsigned char data[16];
} mpcan_ext_hw_access;
/// \typedef mpcan_ext_hw_access
/// Data type used to tranfer data for user command hardware access functions


/// \def MPCAN_SEMAPHORE_SET_HANDLE
/// This macro defines a code used in member semmAccess of struct _mpcan_sem_desc. It is
/// used to signal the driver, that the contained data in the data structure shall be
/// used to define a new semaphore object.
#define MPCAN_SEMAPHORE_SET_HANDLE		0x01

/// \def MPCAN_SEMAPHORE_DEL_HANDLE
/// This macro defines a code used in member semmAccess of struct _mpcan_sem_desc. It is
/// used to signal the driver, that the contained data in the data structure shall be
/// used to delete an old semaphore object.
#define MPCAN_SEMAPHORE_DEL_HANDLE		0x02

/// \struct _mpcan_sem_desc
/// The data structure was defined for transferring semaphore data from the host application
/// to the driver. If the host application wants to be notified about interrupts for the device
/// it has to declare an semaphore object to the driver. The driver will increase this object
/// each time an interrupt occurred, so that the application can do, what it needs to do.
typedef struct _mpcan_sem_desc
{
	/// \var header
	/// Contains header information which are common to all data packages
	/// exchanged between host application and driver
	mpcan_ioctl_header header;
	/// \var semAccess
	/// Signals whether the semaphore specified in hSem is a new one which needs
	/// to be declared to the driver or whether it shall be deleted.
	unsigned long semAccess;
	/// \var hSem
	/// Handle to semaphore object
	void *hSem;
} mpcan_sem_desc;
/// \typedef mpcan_sem_desc
/// Data type used to tranfer semaphore data to the device driver



typedef struct _mpcan_sem_desc32
{
	/// \var header
	/// Contains header information which are common to all data packages
	/// exchanged between host application and driver
	mpcan_ioctl_header header;
	/// \var semAccess
	/// Signals whether the semaphore specified in hSem is a new one which needs
	/// to be declared to the driver or whether it shall be deleted.
	unsigned long semAccess;
	/// \var hSem
	/// Handle to semaphore object
	unsigned __int32 hSem;					// Semaphore for 32Bit
} mpcan_sem_desc32;
/// \typedef mpcan_sem_desc
/// Data type used to tranfer semaphore data to the device driver




/// \struct _mpcan_idvers_desc
/// The data structure was defined for transferring device driver version information from
/// the driver to the host application.
typedef struct _mpcan_idvers_desc
{
	/// \var header
	/// Contains header information which are common to all data packages
	/// exchanged between host application and driver
	mpcan_ioctl_header header;
	/// \var carrierDriverMajorNumber
	/// Major release number of the carrier device driver
	unsigned char carrierDriverMajorNumber;
	/// \var carrierDriverMinorNumber
	/// Minor release number of the carrier device driver
	unsigned char carrierDriverMinorNumber;
	/// \var carrierDriverBuildNumber
	/// Build release number of the carrier device driver, this number
	/// will be increased from compiler run to compiler run. It do not
	/// start from 0 again when major or minor release numbers are
	/// changed.
	unsigned short carrierDriverBuildNumber;
	/// \var mpcanDriverMajorNumber
	/// Major release number of the carrier device driver
	unsigned char mpcanDriverMajorNumber;
	/// \var mpcanDriverMinorNumber
	/// Minor release number of the carrier device driver
	unsigned char mpcanDriverMinorNumber;
	/// \var carrierDriverBuildNumber
	/// Build release number of the carrier device driver, this number
	/// will be increased from compiler run to compiler run. It do not
	/// start from 0 again when major or minor release numbers are
	/// changed.
	unsigned short mpcanDriverBuildNumber;
} mpcan_idvers_desc;
/// \typedef mpcan_idvers_desc
/// Data type used to get version information from the device driver


/// Macro defining the number of messages which can be received max. in one 
/// reception operation
#define PCAN_MAX_MESSAGES_IN_BUFFER		20

/// \struct _mpcan_hw_message
/// The data structure was defined for 
typedef struct _mpcan_hw_message
{
	/// \var header
	/// Contains header information which are common to all data packages
	/// exchanged between host application and driver
	mpcan_ioctl_header header;
	/// var \n
	/// returns the number of messages saved in the following buffer
	/// area, less or equal the value defined macro PCAN_MAX_MESSAGES_IN_BUFFER
	unsigned long n;
	/// \var msg
	/// first PCAN message data block, further blocks may follow
	pcanVoidMsg_t msg[PCAN_MAX_MESSAGES_IN_BUFFER];
} mpcan_hw_message;
/// \typedef mpcan_ext_hw_access
/// Data type used to tranfer PCAN message data to the driver


// Defines the prototype of a callback function. An application can register such a 
// callback function in the library. The library will setup a background thread which
// waits for messages sent by the device. This thread will call the callback function
// when messages were received.

typedef void (*pcan_callback_func) (int fd, pcanVoidMsg_t *pMsg);


/* -*-Struct-*-
 *
 * pcan_can_msg_data - data contained in a CAN message frame
 *
 * This data structure provides a easy CAN message data format. Data is given to
 * and received from the library in this format. Conversion to the format how
 * data is sent to the device will be done internally.
 *
 */
typedef struct __pcan_can_msg_data
{
	// CAN message identifier
	unsigned long id;
	// RTR bit, 0 = not set, 1 = set
	char rtr;
	// Extended idententifier flag, 1 = ext. identifier, 0 = std. identifier
	char extId;
	// Data length
	char len;
	// CAN message data 
	char data[8];
} pcan_can_msg_data;



//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//
// IOCTL CODE DEFINITIONS
//

/// \def MPCAN_IOCTL_IDVERS
/// IOCTL code getting device driver version information
#define MPCAN_IOCTL_IDVERS					CTL_CODE(FILE_DEVICE_UNKNOWN, 0x900, METHOD_BUFFERED, FILE_ANY_ACCESS)
/// \def MPCAN_IOCTL_READ
/// IOCTL code for reading data from the device
#define MPCAN_IOCTL_READ					CTL_CODE(FILE_DEVICE_UNKNOWN, 0x901, METHOD_BUFFERED, FILE_ANY_ACCESS)
/// \def MPCAN_IOCTL_WRITE
/// IOCTL code for writing  data to the device
#define MPCAN_IOCTL_WRITE					CTL_CODE(FILE_DEVICE_UNKNOWN, 0x902, METHOD_BUFFERED, FILE_ANY_ACCESS)
/// \def MPCAN_IOCTL_SEMAPHORE
/// IOCTL code for declaring a semaphore handle to the driver
#define MPCAN_IOCTL_SEMAPHORE				CTL_CODE(FILE_DEVICE_UNKNOWN, 0x903, METHOD_BUFFERED, FILE_ANY_ACCESS)
/// \def MPCAN_IOCTL_DUMP_REG
/// IOCTL code reading complete register contents into buffer and return them to the driver
#define MPCAN_IOCTL_DUMP_REG				CTL_CODE(FILE_DEVICE_UNKNOWN, 0x904, METHOD_BUFFERED, FILE_ANY_ACCESS)
/// \def MPCAN_IOCTL_GET_QUEUE_STATUS
/// IOCTL code getting information about drivers queues
#define MPCAN_IOCTL_GET_QUEUE_STATUS		CTL_CODE(FILE_DEVICE_UNKNOWN, 0x905, METHOD_BUFFERED, FILE_ANY_ACCESS)

#endif   // #ifndef __JMPCAN_H__
