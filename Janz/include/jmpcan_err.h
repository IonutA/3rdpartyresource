///////////////////////////////////////////////////////////////////////////////
///
/// \file jmpcan_err.h
/// Header file which defines macros for return codes.
///
/// The file contains some macros defining values which are used as return codes
/// in library and device driver functions. The comment will give you a short
/// description about when the corresponding return code will be used.
///
///////////////////////////////////////////////////////////////////////////////

#ifndef __JMPCAN_ERR_H__
#define __JMPCAN_ERR_H__

/// The following macros define values returned in error situations
/// from the driver as well as from the library

/// No error.
#define MPCAN_ERR_SUCCESS											 0

/// An error has occurred.
#define MPCAN_ERR_ERROR											-1

/// The corresponding IOCTL call has failed.
#define MPCAN_ERR_IOCTL_FAILED									-2

/// A device connection needed for the function
/// was not opened before.
#define MPCAN_ERR_DEVICE_NOT_OPEN									-3

/// The driver operation has failed, check the return code in
/// the ioctl buffer.
#define MPCAN_ERR_DRIVER_ACTION_FAILED                            -4

/// A function has detected a wrong value for its parameters.
#define MPCAN_ERR_WRONG_PARAMETER									-5

/// Error occurred in the device driver.
#define MPCAN_ERR_DRIVER_ERROR                                    -6

/// A NULL pointer was detected as function argument.
#define MPCAN_ERR_NULL_POINTER_DETECTED							-7

/// An error occurred which normally should not occur,
/// please contact Janz support.
#define MPCAN_ERR_FATAL_ERROR										-8

/// A feature should be configured which is already in use.
#define MPCAN_ERR_ALREADY_IN_USE									-9

/// Wrong module number was detected.
#define MPCAN_ERR_INVALID_MODULE_NUMBER							-10

/// Specified version does not match.
#define MPCAN_ERR_WRONG_VERSION									-11

/// Error was detected in specified resource pattern,
/// function ican_check_dpm_resources returned an error.
#define MPCAN_ERR_RESOURCE_CONFLICT								-12

/// a timeout occurred
#define MPCAN_ERR_TIMEOUT										-13

/// Loading a library has failed
#define MPCAN_ERR_LOAD_LIBRARY									-14

/// Loading a function pointer from a library has failed
#define MPCAN_ERR_LOAD_FUNCTION									-15

/// function is not supported by the currently installed version of the library 
#define MPCAN_ERR_NOT_SUPPORTED									-16

/// no data was found for the request
#define MPCAN_ERR_NO_DATA										-17

/// someone tried to write a buffer to a queue which is already full
#define MPCAN_ERR_QUEUE_OVERFLOW								-18

/// a wrong mode was deteted
#define MPCAN_ERR_WRONG_MODE									-19

/// Opening a device connection has failed
#define MPCAN_ERR_DEVICE_OPEN_FAILED							-20

#endif   // #ifndef __JMPCAN_ERR_H__
