///////////////////////////////////////////////////////////////////////////////
///
/// \file jmpcanlib.h
/// Header file which defines function prototypes exported by the device
/// access library.
///
///////////////////////////////////////////////////////////////////////////////

#if __cplusplus
extern "C" {
#endif

#include <windows.h>
#ifdef JPCAN_EXPORTS
#define JPCAN_API __declspec(dllexport)
#else
#define JPCAN_API __declspec(dllimport)
#endif

/***** Macro definitions */

/// Macro used in function pcan_led as third parameter for switching a LED off
#define MPCAN_LED_OFF			0
/// Macro used in function pcan_led as third parameter for switching a LED on
#define MPCAN_LED_ON			1
/// Macro used in function pcan_led as second parameter for addressing the green LED
/// on the device
#define MPCAN_GREEN_LED			1
/// Macro used in function pcan_led as second parameter for addressing the red LED
/// on the device
#define MPCAN_RED_LED			0

/***** Library function forward declarations */

/* Function for opening device connection */
JPCAN_API int pcan_open (char *deviceName);
/* Function for closing device connection */
JPCAN_API long pcan_close (int fd);

/* Function for connecting device with the bus */
JPCAN_API long pcan_buson(int fd);
/* Function for disconnecting device with the bus */
JPCAN_API long pcan_busoff(int fd);

/* Function for setting CAN bus bit rate to use */
JPCAN_API long pcan_setbtr(int fd, unsigned short bitrate);
/* Function for setting internal CAN bus termination of the device */
JPCAN_API long pcan_config_term(int fd, unsigned char term);

/* Function for printing a program header on stderr, used in examples programs */
JPCAN_API void pcan_print_example_header (char *progName);

/* Function for sending CAN message data on the bus */
JPCAN_API long pcan_send_msg (int fd, pcan_can_msg_data *pMsgData);
JPCAN_API long pcan_send_prio_msg (int fd, pcan_can_msg_data *pMsgData, unsigned int prio);

/* Function for getting  CAN message data out of device's internal queues */
JPCAN_API long pcan_get_msg (int fd, pcan_can_msg_data *pMsgData);

/* function for inquuiring device status */
JPCAN_API long pcan_inquiry_status (int fd, pcanCtrlMsg_t *rbuffer);
/* function for inquuiring number of messages saved in queues */
JPCAN_API long pcan_inquiry_poolstat (int index, unsigned int *puiBuffer, int iBufferLength);
/* function for inquuiring busload information from the device */
JPCAN_API long pcan_inquiry_busload(int index, pcanCtrlMsg_t *rbuffer);


/* function for configuring message filtering on the device */
JPCAN_API long pcan_set_afilmask (int fd, unsigned long idl, unsigned long idh, unsigned long mask);
/* function for setting filter options */
JPCAN_API long pcan_conf_afilopt (int fd, unsigned char opt);
/* function for locking filter mechanism */
JPCAN_API long pcan_lock_afil (int fd, unsigned char lockreq);

/* function for configuring SNIFF buffer capabilities */
JPCAN_API long pcan_config_sniff (int fd, unsigned short mode);
/* function for setting error warning level value */
JPCAN_API long pcan_config_ewl (int fd, unsigned char ewl);
/* function for setting error quota value */
JPCAN_API long pcan_config_berr (int fd, unsigned char quota);
/* function for activating self-test mode on the device */
JPCAN_API long pcan_config_stm (int fd, unsigned char mode);
/* function for activating listen-only mode on the device */
JPCAN_API long pcan_config_lom (int fd, unsigned char mode);
/* function for configuring acceptance filter */
JPCAN_API long pcan_config_acm (int fd, unsigned char mode, unsigned long ac, unsigned long am);

/* function for setting message to send when sync event should occur */
JPCAN_API long pcan_set_sync_msg (int fd, pcan_can_msg_data *p);
/* function for setting time delay between two sync messages */
JPCAN_API long pcan_set_sync_period (int fd, unsigned long period);

/* function for converting PCAN message data to text format (DEBUG FUNCTION!) */
JPCAN_API long pcan_sprintMsg (char *d, const char *fmt, pcanVoidMsg_t *p);

/* Function for initializing buffer pool in the driver */
JPCAN_API long pcan_init_pool(int fd, unsigned long size);

/* Function for registering a callback function in the library called when message data
 * is available
 */
JPCAN_API long pcan_register_callback (int fd, pcan_callback_func pFunc);
JPCAN_API long pcan_register_callback_ex (int fd, pcan_callback_func pFunc, int prio);

/* Function for deregistering the callback function in the library */
JPCAN_API long pcan_deregister_callback (int fd);

/* function for transmitting raw PCAN buffers to and from the device */
JPCAN_API long pcan_get_buffer (int fd, void *buffer, unsigned long size);
JPCAN_API long pcan_write_buffer (int fd, unsigned char *buffer, unsigned long size);

/* function for requesting library, device driver and firmware version information */
JPCAN_API long pcan_idvers (int fd, unsigned long *modVers, unsigned long *carVers);

/* function for printing a common header for all example programs */
JPCAN_API void pcan_print_example_header (char *progName);

/* function for turning LEDs on and off */
JPCAN_API long pcan_led (int fd, unsigned char ledNo, unsigned char status);

/* macro defined for source code compatibility with other operating systems */
#define pcan_set_led pcan_led

JPCAN_API long pcan_dump_reg (int fd, int start, unsigned char *pBuffer, int size);

/* Function for getting CAN message data in polling mode */
JPCAN_API long pcan_poll_msg (int fd, pcanVoidMsg_t *pBuffer, int size);

// data structure containing CAN-USB device information
typedef struct __pcan_usb_dev_info
{
	// device name
	char szDevName[32];
	// serial number string
	char szSerialNumber[32];
	// device flags
	unsigned long ulFlags;
	// device type
	unsigned long ulType;
	// device ID
	unsigned long ulID;
	// device location ID
	unsigned long ulLocId;
	// device descrition string
	char szDescription[64];
	// device connection handle
	void *pHandle;

} pcan_usb_dev_info;



// ama 2013-07

// data structure containing mpcan device information
typedef struct __pcan_dev_info
{
	// device name
	char szDevName[32];
	
} pcan_dev_info;




// Returns a list containing device properties of CAN-USB devices. 
JPCAN_API long pcan_list_usb_devices (pcan_usb_dev_info **pNodes, unsigned char *pNumDevs);

// Free memory allocated in pcan_list_usb_devices
JPCAN_API long pcan_list_usb_devices_free (pcan_usb_dev_info **pNodes);

// Returns a list of device information containing available mpcan device names. 
JPCAN_API long pcan_list_devices (pcan_dev_info **pNodes, unsigned char *pNumDevs);

// Free memory allocated in pcan_list_devices
JPCAN_API long pcan_list_devices_free (pcan_dev_info **pNodes);


#ifdef WIN32

JPCAN_API long pcan_inquiry_rxqueuestatus (int fd, unsigned long *pRxQueueStatus);

#endif

#if __cplusplus
}
#endif
