QT_BUILD_PARTS += libs tools
QT_QCONFIG_PATH = 

host_build {
    QT_CPU_FEATURES.x86_64 =  sse sse2
} else {
    QT_CPU_FEATURES.x86_64 =  sse sse2
}
QT_COORD_TYPE += double
CONFIG += precompile_header pcre debug compile_examples sse2 sse3 ssse3 sse4_1 sse4_2 avx avx2 largefile
sql-drivers    += sqlite
styles         += windows fusion windowsxp windowsvista
