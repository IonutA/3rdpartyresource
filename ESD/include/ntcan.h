/**************************************************************************
*                                                                         *
* ntcan.h -- NTCAN-API procedure declarations and constant definitions    *
*                                                                         *
* Copyright (c) 1997-2013, esd gmbh. All rights reserved.                 *
*                                                                         *
**************************************************************************/

#pragma once

#ifndef _ntcan_h_
#define _ntcan_h_

#ifdef __cplusplus
extern "C" {
#endif

#include <windows.h>
#if defined(__MINGW32__) || defined (__MINGW64__)
# include <stdint.h>      /* Include stdint.h if available */
#endif

#if !defined(EXPORT)
# define EXPORT __declspec (dllexport)    /* Macro to export from DLL */
#endif

#if !defined(CALLTYPE)
# define CALLTYPE __cdecl
#endif

#ifdef _MSC_VER
# ifdef UNDER_RTSS
# pragma comment(lib,"ntcan_rtss.lib")  /* Link with NTCAN_RTSS.LIB (Only for VC++)*/
# elif defined(RTOS32)
    /* RTOS32 */
# else
#  pragma comment(lib,"ntcan.lib")       /* Link with NTCAN.LIB (Only for VC++)*/
# endif
#endif

/*
 * Macros for compiler warnings if obsolete API functions are used (VC++ only).
 * You can prevent the warnings by defining _NTCAN_OBSOLETE_NO_WARNINGS before
 * the NTCAN header is included.
 */
#if defined(_MSC_VER) && _MSC_VER >= 1300
# if _MSC_FULL_VER >= 140050320
#  define _NTCAN_DEPRECATE_TEXT(_Text) __declspec(deprecated(_Text))
# else
#  define _NTCAN_DEPRECATE_TEXT(_Text) __declspec(deprecated)
# endif
# ifndef _NTCAN_OBSOLETE
#  ifdef _NTCAN_OBSOLETE_NO_WARNINGS
#   define _NTCAN_OBSOLETE(_NewItem)
#  else
#   define _NTCAN_OBSOLETE(_NewItem) \
        _NTCAN_DEPRECATE_TEXT("This function has been superceded by newer" \
                              "library functionality. Consider using "     \
                              "#_NewItem instead. See API manual for details.")
#  endif /* of defined(_NTCAN_OBSOLETE_NO_WARNINGS) */
# endif /* of !defined(_NTCAN_OBSOLETE) */
# else
#  define _NTCAN_OBSOLETE(_NewItem)
#endif /* of if defined(_MSC_VER) && _MSC_VER >= 1300 */

/*------------------ Defines ------------------------------------------------*/
#define NTCAN_EV_BASE                   0x40000000
#define NTCAN_EV_USER                   0x40000080
#define NTCAN_EV_LAST                   0x400000FF

#define NTCAN_EV_CAN_ERROR              NTCAN_EV_BASE
#define NTCAN_EV_BAUD_CHANGE           (NTCAN_EV_BASE + 0x1)
#define NTCAN_EV_CAN_ERROR_EXT         (NTCAN_EV_BASE + 0x2)
#define NTCAN_EV_BUSLOAD               (NTCAN_EV_BASE + 0x3)

/*
 * Handle specific flags in canOpen()
 */
#define NTCAN_MODE_NO_RTR               0x00000010  /* Ignore RTR frames      */
#define NTCAN_MODE_NO_DATA              0x00000020  /* Ignore data frames     */
#define NTCAN_MODE_NO_INTERACTION       0x00000100  /* No interaction         */
#define NTCAN_MODE_MARK_INTERACTION     0x00000200  /* Mark interaction       */
#define NTCAN_MODE_LOCAL_ECHO           0x00000400  /* Local echo             */
#define NTCAN_MODE_TIMESTAMPED_TX       0x00020000  /* Timestamped TX         */
#define NTCAN_MODE_OVERLAPPED           0x20000000  /* Open for overlapped    */
#define NTCAN_MODE_OBJECT               0x10000000  /* Open in Rx object mode */

/* Queue-Size in canOpen() */
#define NTCAN_MAX_TX_QUEUESIZE          16383
#define NTCAN_MAX_RX_QUEUESIZE          16383
#define NTCAN_NO_QUEUE                  -1

/* Max for parameter net canOpen() */
#define NTCAN_MAX_NETS                  255

/* Flags in can-ids */
#define NTCAN_20B_BASE                  0x20000000

/* Flags in frame-length's  */
#define NTCAN_RTR                       0x10     /* CAN message is RTR       */
#define NTCAN_NO_DATA                   0x20     /* Supported in object mode */
#define NTCAN_INTERACTION               0x20     /* Supported in FIFO mode   */

/* Defines for canSetBaudrate() / canGetBaudrate() */
#define NTCAN_USER_BAUDRATE             0x80000000  /* Configure BTR register     */
#define NTCAN_LISTEN_ONLY_MODE          0x40000000  /* Configure listen only mode */
#define NTCAN_USER_BAUDRATE_NUM         0x20000000  /* Numerical baudrate         */
#define NTCAN_AUTOBAUD                  0x00FFFFFE  /* Auto baudrate detection    */
#define NTCAN_NO_BAUDRATE               0x7FFFFFFF  /* No baudrate configured     */

/*
 * Predefined CiA-recommended baudrates for canSetBaudrate() / canGetBaudrate()
 * of the ESD baudrate table
 */
#define NTCAN_BAUD_1000                   0x00
#define NTCAN_BAUD_800                 0x0E
#define NTCAN_BAUD_500                 0x02
#define NTCAN_BAUD_250                 0x04
#define NTCAN_BAUD_125                 0x06
#define NTCAN_BAUD_100                 0x07
#define NTCAN_BAUD_50                  0x09
#define NTCAN_BAUD_20                  0x0B
#define NTCAN_BAUD_10                  0x0D

/*
 * Flags for scheduling mode
 */
#define NTCAN_SCHED_FLAG_EN            0x00000000 /* ID is enabled          */
#define NTCAN_SCHED_FLAG_DIS           0x00000002 /* ID is disabled         */
#define NTCAN_SCHED_FLAG_REL           0x00000000 /* Start time is relative */
#define NTCAN_SCHED_FLAG_ABS           0x00000001 /* Start time is absolute */
#define NTCAN_SCHED_FLAG_INC8          0x00000100 /*  8 Bit incrementer     */
#define NTCAN_SCHED_FLAG_INC16         0x00000200 /* 16 Bit incrementer     */
#define NTCAN_SCHED_FLAG_INC32         0x00000300 /* 32 Bit incrementer     */
#define NTCAN_SCHED_FLAG_DEC8          0x00000400 /*  8 Bit decrementer     */
#define NTCAN_SCHED_FLAG_DEC16         0x00000500 /* 16 Bit decrementer     */
#define NTCAN_SCHED_FLAG_DEC32         0x00000600 /* 32 Bit decrementer     */
#define NTCAN_SCHED_FLAG_OFS0          0x00000000 /* Counter at offset 0    */
#define NTCAN_SCHED_FLAG_OFS1          0x00001000 /* Counter at offset 1    */
#define NTCAN_SCHED_FLAG_OFS2          0x00002000 /* Counter at offset 2    */
#define NTCAN_SCHED_FLAG_OFS3          0x00003000 /* Counter at offset 3    */
#define NTCAN_SCHED_FLAG_OFS4          0x00004000 /* Counter at offset 4    */
#define NTCAN_SCHED_FLAG_OFS5          0x00005000 /* Counter at offset 5    */
#define NTCAN_SCHED_FLAG_OFS6          0x00006000 /* Counter at offset 6    */
#define NTCAN_SCHED_FLAG_OFS7          0x00007000 /* Counter at offset 7    */

/* CAN controller types
 * (returned by canIoctl(NTCAN_IOCTL_GET_CTRL_STATUS) or
 *  canIoctl(NTCAN_IOCTL_GET_BITRATE_DETAILS)) */
#define NTCAN_CANCTL_SJA1000     0x00  /* NXP SJA1000 / 82C200               */
#define NTCAN_CANCTL_I82527      0x01  /* Intel I82527                       */
#define NTCAN_CANCTL_FUJI        0x02  /* Fujitsu MBxxxxx MCU                */
#define NTCAN_CANCTL_LPC         0x03  /* NXP LPC2xxx / LPC17xx MCU          */
#define NTCAN_CANCTL_MSCAN       0x04  /* Freescale MCU                      */
#define NTCAN_CANCTL_ATSAM       0x05  /* Atmel ARM CPU                      */
#define NTCAN_CANCTL_ESDACC      0x06  /* esd Advanced CAN Core              */
#define NTCAN_CANCTL_STM32       0x07  /* ST STM32Fxxx MCU (bxCAN)           */
#define NTCAN_CANCTL_CC770       0x08  /* Bosch CC770 (82527 compatible)     */
#define NTCAN_CANCTL_SPEAR       0x09  /* ST SPEAr320 (C_CAN)                */
#define NTCAN_CANCTL_FLEXCAN     0x0A  /* Freescale iMX MCU (FlexCAN)        */
#define NTCAN_CANCTL_SITARA      0x0B  /* TI AM335x (Sitara) MCU (D_CAN)     */
#define NTCAN_CANCTL_MCP2515     0x0C  /* Microchip MCP2515                  */

/* CAN baudrate flags
 * (returned by canIoctl(NTCAN_IOCTL_GET_BITRATE_DETAILS),
 *  see NTCAN_BITRATE structure (flags)) */
#define NTCAN_BITRATE_FLAG_SAM  0x00000001

/*
 * Return values.
 */
#define NTCAN_SUCCESS                   ERROR_SUCCESS
#define NTCAN_RX_TIMEOUT                0xE0000001
#define NTCAN_TX_TIMEOUT                0xE0000002
#define NTCAN_TX_ERROR                  0xE0000004
#define NTCAN_CONTR_OFF_BUS             0xE0000005
#define NTCAN_CONTR_BUSY                0xE0000006
#define NTCAN_CONTR_WARN                0xE0000007
#define NTCAN_NO_ID_ENABLED             0xE0000009
#define NTCAN_ID_ALREADY_ENABLED        0xE000000A
#define NTCAN_ID_NOT_ENABLED            0xE000000B

#define NTCAN_INVALID_FIRMWARE          0xE000000D
#define NTCAN_MESSAGE_LOST              0xE000000E
#define NTCAN_INVALID_HARDWARE          0xE000000F

#define NTCAN_PENDING_WRITE             0xE0000010
#define NTCAN_PENDING_READ              0xE0000011
#define NTCAN_INVALID_DRIVER            0xE0000012    /* wrong dll/driver-combination */

#define NTCAN_CONTR_ERR_PASSIVE         0xE0000016
#define NTCAN_ERROR_NO_BAUDRATE         0xE0000017
#define NTCAN_ERROR_LOM                 0xE0000018

#define NTCAN_SOCK_CONN_TIMEOUT         0xE0000080
#define NTCAN_SOCK_CMD_TIMEOUT          0xE0000081
#define NTCAN_SOCK_HOST_NOT_FOUND       0xE0000082    /* gethostbyname() failed */

#define NTCAN_INVALID_PARAMETER         ERROR_INVALID_PARAMETER
#define NTCAN_INVALID_HANDLE            ERROR_INVALID_HANDLE
#define NTCAN_IO_INCOMPLETE             ERROR_IO_INCOMPLETE
#define NTCAN_IO_PENDING                ERROR_IO_PENDING
#define NTCAN_NET_NOT_FOUND             ERROR_FILE_NOT_FOUND
#define NTCAN_INSUFFICIENT_RESOURCES    ERROR_NO_SYSTEM_RESOURCES

#define NTCAN_OPERATION_ABORTED         ERROR_OPERATION_ABORTED
#define NTCAN_WRONG_DEVICE_STATE        ERROR_NOT_READY
#define NTCAN_HANDLE_FORCED_CLOSE       ERROR_HANDLE_EOF
#define NTCAN_NOT_IMPLEMENTED           ERROR_INVALID_FUNCTION
#define NTCAN_NOT_SUPPORTED             ERROR_NOT_SUPPORTED

/*------------------ Macros ------------------------------------------------*/

    /* Macros to decode the 32-bit boardstatus of CAN_IF_STATUS */
#define NTCAN_GET_CTRL_TYPE(boardstatus)     ((boardstatus >> 24) & 0xFF)
#define NTCAN_GET_BOARD_STATUS(boardstatus)  (boardstatus & 0xFFFF)

    /* Macros for parameter len of CMSG / CMSG_T structure */
#define NTCAN_DLC(len)               ((len) & 0x0F)
#define NTCAN_DLC_AND_TYPE(len)      ((len) & (0x0F | NTCAN_RTR))
#define NTCAN_IS_RTR(len)            ((len) & NTCAN_RTR)
#define NTCAN_IS_INTERACTION(len)    ((len) & NTCAN_INTERACTION)

    /*
     * POSIX data types usually not defined in Windows so we define them
     * now to prevent warnings
     */
#ifndef _DATA_TYPE_ 
#define _DATA_TYPE_
#ifndef _STDINT_H
# ifndef _WIN64
typedef unsigned char     uint8_t;
typedef signed   char     int8_t;
typedef unsigned short    uint16_t;
typedef signed   short    int16_t;
typedef unsigned int      uint32_t;
typedef signed   int      int32_t;
# else
typedef unsigned __int8   uint8_t;
/*typedef signed __int8     int8_t;*/
typedef unsigned __int16  uint16_t;
typedef signed __int16    int16_t;
typedef unsigned __int32  uint32_t;
typedef signed __int32    int32_t;
# endif /* of _WIN64 */
typedef unsigned __int64  uint64_t;
typedef signed __int64    int64_t;
#endif /* of _STDINT_H */
#endif

typedef DWORD             NTCAN_RESULT;
typedef HANDLE            NTCAN_HANDLE;

#pragma pack(1)

typedef struct
{
  int32_t       id;             /* can-id                                   */
  uint8_t       len;            /* length of message: 0-8                   */
  uint8_t       msg_lost;       /* count of lost rx-messages                */
  uint8_t       reserved[2];    /* reserved                                 */
  uint8_t       data[8];        /* 8 data-bytes                             */
} CMSG;

typedef struct
{
  int32_t       id;             /* can-id                                    */
  uint8_t       len;            /* length of message: 0-8                    */
  uint8_t       msg_lost;       /* count of lost rx-messages                 */
  uint8_t       reserved[2];    /* reserved                                  */
  uint8_t       data[8];        /* 8 data-bytes                              */
  uint64_t      timestamp;      /* time stamp of this message                */
} CMSG_T;

typedef struct
{
    uint8_t     reserved1;      /* Reserved for future use                   */
    uint8_t     can_status;     /* CAN controller status                     */
    uint8_t     reserved2;      /* Reserved for future use                   */
    uint8_t     ctrl_overrun;   /* Controller overruns                       */
    uint8_t     reserved3;      /* Reserved for future use                   */
    uint8_t     fifo_overrun;   /* Driver FIFO overruns                      */
} EV_CAN_ERROR;

typedef struct
{
    uint32_t    baud;           /* New NTCAN baudrate value                  */
    uint32_t    num_baud;       /* New numerical baudrate value (optional)   */
} EV_CAN_BAUD_CHANGE;

typedef union
{
    struct {
        uint8_t     status;     /* (SJA1000) CAN controller status           */
        uint8_t     ecc;        /* Error Capture Register                    */
        uint8_t     rec;        /* Rx Error Counter                          */
        uint8_t     tec;        /* Tx Error Counter                          */
    } sja1000;
    struct {
        uint8_t     status;     /* (ESDACC) CAN controller status            */
        uint8_t     ecc;        /* Error Capture Register                    */
        uint8_t     rec;        /* Rx Error Counter                          */
        uint8_t     tec;        /* Tx Error Counter                          */
        uint8_t     txstatus;   /* (ESDACC) CAN controller TX status         */
    } esdacc;
} EV_CAN_ERROR_EXT;

typedef struct
{
  int32_t       evid;          /* event-id: possible range:EV_BASE...EV_LAST */
  uint8_t       len;           /* length of message: 0-8                     */
  uint8_t       reserved[3];   /* reserved                                   */
  union
  {
    uint8_t            c[8];
    uint16_t           s[4];
    uint32_t           l[2];
    uint64_t           q;
    EV_CAN_ERROR       error;
    EV_CAN_BAUD_CHANGE baud_change;
    EV_CAN_ERROR_EXT   error_ext;
  } evdata;
} EVMSG;

typedef struct
{
  int32_t       evid;          /* event-id: possible range:EV_BASE...EV_LAST */
  uint8_t       len;           /* length of message: 0-8                     */
  uint8_t       reserved[3];   /* reserved                                   */
  union
  {
    uint8_t            c[8];
    uint16_t           s[4];
    uint32_t           l[2];
    uint64_t           q;
    EV_CAN_ERROR       error;
    EV_CAN_BAUD_CHANGE baud_change;
    EV_CAN_ERROR_EXT   error_ext;
  } evdata;
  uint64_t      timestamp;      /* time stamp of this message                */
} EVMSG_T;

typedef struct
{
  uint16_t hardware;           /* Hardware version                          */
  uint16_t firmware;           /* Firmware version (0 for passive hardware) */
  uint16_t driver;             /* Driver version                            */
  uint16_t dll;                /* NTCAN library version                     */
  uint32_t boardstatus;        /* Hardware status                           */
  uint8_t  boardid[14];        /* Board ID string                           */
  uint16_t features;           /* Device/driver capability flags            */
} CAN_IF_STATUS;

typedef struct {
  uint32_t           std_data;            /* # of std CAN messages    */
  uint32_t           std_rtr;             /* # of std RTR requests    */
  uint32_t           ext_data;            /* # of ext CAN messages    */
  uint32_t           ext_rtr;             /* # of ext RTR requests    */
} NTCAN_FRAME_COUNT;

typedef struct
{
  uint64_t           timestamp;           /* Timestamp                     */
  NTCAN_FRAME_COUNT  rcv_count;           /* # of received frames          */
  NTCAN_FRAME_COUNT  xmit_count;          /* # of transmitted frames       */
  uint32_t           ctrl_ovr;            /* # of controller overruns      */
  uint32_t           fifo_ovr;            /* # of FIFO verflows            */
  uint32_t           err_frames;          /* # of error frames             */
  uint32_t           rcv_byte_count;      /* # of received bytes           */
  uint32_t           xmit_byte_count;     /* # of transmitted bytes        */
  uint32_t           aborted_frames;      /* # of aborted frames           */
  uint32_t           reserved[2];         /* Reserved                      */
  uint64_t           bit_count;           /* # of received bits            */
} NTCAN_BUS_STATISTIC;

typedef struct
{
  uint8_t            rcv_err_counter;     /* Receive error counter         */
  uint8_t            xmit_err_counter;    /* Transmit error counter        */
  uint8_t            status;              /* CAN controller status         */
  uint8_t            type;                /* CAN controller type           */
} NTCAN_CTRL_STATE;

typedef struct
{
    uint32_t  baud;         /* value configured by user via canSetBaudrate()        */
    uint32_t  valid;        /* validity of all _following_ infos                    */
                            /* (-1 = invalid, NTCAN_SUCCESS, NTCAN_NOT_IMPLEMENTED) */
    uint32_t  rate;         /* CAN bitrate in Bit/s                                 */
    uint32_t  clock;        /* frequency of CAN controller                          */
    uint8_t   ctrl_type;    /* CANIO_CANCTL_XXX defines                             */
    uint8_t   tq_pre_sp;    /* number of time quantas before samplep. (SYNC + TSEG1)*/
    uint8_t   tq_post_sp;   /* number of time quantas after samplepoint (TSEG2)     */
    uint8_t   sjw;          /* syncronization jump width in time quantas (SJW)      */
    uint32_t  error;        /* actual deviation of configured baudrate in (% * 100) */
    uint32_t  flags;        /* baudrate flags (possibly ctrl. specific, e.g. SAM)   */
    uint32_t  reserved[3];  /* for future use                                       */
} NTCAN_BITRATE;

typedef struct
{
    uint32_t acr;
    uint32_t amr;
    uint32_t idArea;
} NTCAN_FILTER_MASK;

/* CAN-id-regions */
enum NTCAN_IDS_REGION {    NTCAN_IDS_REGION_20A, NTCAN_IDS_REGION_20B, NTCAN_IDS_REGION_EV };

typedef struct
{
  int32_t  id;
  int32_t  flags;
  uint64_t time_start;
  uint64_t time_interval;
  uint32_t count_start; /* Start value for counting.                          */
  uint32_t count_stop;  /* Stop value for counting. After reaching this value,
                           the counter is loaded with the count_start value.  */
} CSCHED;

typedef struct
{
  uint64_t           timestamp;         /* Timestamp (for busload)          */
  uint64_t           timestamp_freq;    /* Timestamp frequency (for busload)*/
  uint32_t           num_baudrate;      /* Numerical baudrate (for busload) */
  uint32_t           flags;             /* Flags                            */
  uint64_t           busload_oldts;     /* <---+-- used internally, set to  */
  uint64_t           busload_oldbits;   /* <---+   zero on first call       */
  uint8_t            ctrl_type;         /* Controller type (for ext_error)  */
  uint8_t            reserved[7];       /* Reserved (7 bytes)               */
  uint32_t           reserved2[4];      /* Reserved (16 bytes)              */
} NTCAN_FORMATEVENT_PARAMS;

#pragma pack()

/*
 * Interface/Driver feature flags
 */
#define NTCAN_FEATURE_FULL_CAN         (1<<0)  /* Full CAN controller                */
#define NTCAN_FEATURE_CAN_20B          (1<<1)  /* CAN 2.OB support                   */
#define NTCAN_FEATURE_DEVICE_NET       (1<<2)  /* Device net adapter                 */
#define NTCAN_FEATURE_CYCLIC_TX        (1<<3)  /* Cyclic Tx support                  */
#define NTCAN_FEATURE_TIMESTAMPED_TX   (1<<3)  /* SAME AS CYCLIC_TX, timestamped TX support */
#define NTCAN_FEATURE_RX_OBJECT_MODE   (1<<4)  /* Rx object mode support             */
#define NTCAN_FEATURE_TIMESTAMP        (1<<5)  /* Timestamp support                  */
#define NTCAN_FEATURE_LISTEN_ONLY_MODE (1<<6)  /* Listen-only-mode support           */
#define NTCAN_FEATURE_SMART_DISCONNECT (1<<7)  /* Go-from-bus-after-last-close       */
#define NTCAN_FEATURE_LOCAL_ECHO       (1<<8)  /* Interaction w. local echo          */
#define NTCAN_FEATURE_SMART_ID_FILTER  (1<<9)  /* Adaptive ID filter                 */
#define NTCAN_FEATURE_SCHEDULING       (1<<10) /* Scheduling feature                 */
#define NTCAN_FEATURE_DIAGNOSTIC       (1<<11) /* CAN bus diagnostig support         */
#define NTCAN_FEATURE_ERROR_INJECTION  (1<<12) /* esdACC error injection support     */
#define NTCAN_FEATURE_IRIGB            (1<<13) /* IRIG-B support                     */
#define NTCAN_FEATURE_PXI              (1<<14) /* Board supports backplane clock and startrigger for timestamp */
#define NTCAN_FEATURE_RESERVED         (1<<15) /* reserved for extension of feature mask */

/* Bus states delivered by status-event (NTCAN_EV_CAN_ERROR) and
*  canIoctl(NTCAN_IOCTL_GET_CTRL_STATUS)
*/
#define NTCAN_BUSSTATE_OK           0x00
#define NTCAN_BUSSTATE_WARN         0x40
#define NTCAN_BUSSTATE_ERRPASSIVE   0x80
#define NTCAN_BUSSTATE_BUSOFF       0xC0

/*
 * IOCTL codes for canIoctl()
 */
#define NTCAN_IOCTL_FLUSH_RX_FIFO         0x0001   /* Flush Rx FIFO           */
#define NTCAN_IOCTL_GET_RX_MSG_COUNT      0x0002   /* Ret # CMSG in Rx FIFO   */
#define NTCAN_IOCTL_GET_RX_TIMEOUT        0x0003   /* Ret configured Rx tout  */
#define NTCAN_IOCTL_GET_TX_TIMEOUT        0x0004   /* Ret configured Tx tout  */
#define NTCAN_IOCTL_SET_20B_HND_FILTER    0x0005   /* Set 2.0B filter mask    */
#define NTCAN_IOCTL_GET_SERIAL            0x0006   /* Get HW serial number    */
#define NTCAN_IOCTL_GET_TIMESTAMP_FREQ    0x0007   /* Get timestamp frequency in Hz  */
#define NTCAN_IOCTL_GET_TIMESTAMP         0x0008   /* Get timestamp counter   */
#define NTCAN_IOCTL_ABORT_RX              0x0009   /* Abort pending Rx I/O    */
#define NTCAN_IOCTL_ABORT_TX              0x000A   /* Abort pending Tx I/O    */
#define NTCAN_IOCTL_SET_RX_TIMEOUT        0x000B   /* Change Rx timeout       */
#define NTCAN_IOCTL_SET_TX_TIMEOUT        0x000C   /* Change Rx timeout       */
#define NTCAN_IOCTL_TX_OBJ_CREATE         0x000D   /* Create obj, arg->CMSG          */
#define NTCAN_IOCTL_TX_OBJ_AUTOANSWER_ON  0x000E   /* Switch autoanswer on,arg->CMSG */
#define NTCAN_IOCTL_TX_OBJ_AUTOANSWER_OFF 0x000F   /* Switch autoanswer off,arg->CMSG*/
#define NTCAN_IOCTL_TX_OBJ_UPDATE         0x0010   /* update  obj, arg->CMSG         */
#define NTCAN_IOCTL_TX_OBJ_DESTROY        0x0011   /* Destroy obj, arg->id           */

#define NTCAN_IOCTL_TX_OBJ_SCHEDULE_START 0x0013   /* Start scheduling for handle    */
#define NTCAN_IOCTL_TX_OBJ_SCHEDULE_STOP  0x0014   /* Stop scheduling for handle     */
#define NTCAN_IOCTL_TX_OBJ_SCHEDULE       0x0015   /* Set sched. for obj,arg->CSCHED */
#define NTCAN_IOCTL_SET_BUSLOAD_INTERVAL  0x0016   /* Set busload event interval (ms)*/
#define NTCAN_IOCTL_GET_BUSLOAD_INTERVAL  0x0017   /* Get busload event interval (ms)*/
#define NTCAN_IOCTL_GET_BUS_STATISTIC     0x0018   /* Get CAN bus statistic          */
#define NTCAN_IOCTL_GET_CTRL_STATUS       0x0019   /* Get Controller status          */
#define NTCAN_IOCTL_GET_BITRATE_DETAILS   0x001A   /* Get detailed baudrate info     */
#define NTCAN_IOCTL_GET_NATIVE_HANDLE     0x001B   /* Get native (OS) handle         */
#define NTCAN_IOCTL_SET_HND_FILTER        0x001C   /* Set hnd filter mask            */
/* Error injection IOCTLs */
#define NTCAN_IOCTL_EEI_CREATE            0x0020   /* Allocate esdacc error injection (EEI) unit */
#define NTCAN_IOCTL_EEI_DESTROY           0x0021   /* Free EEI unit                              */
#define NTCAN_IOCTL_EEI_STATUS            0x0022   /* Get status of EEI unit                     */
#define NTCAN_IOCTL_EEI_CONFIGURE         0x0023   /* Configure EEI unit                         */
#define NTCAN_IOCTL_EEI_START             0x0024   /* Arm EEI unit                               */
#define NTCAN_IOCTL_EEI_STOP              0x0025   /* Halt EEI unit                              */
#define NTCAN_IOCTL_EEI_TRIGGER_NOW       0x0026   /* Manually trigger EEI unit                  */

#define NTCAN_IOCTL_SET_TX_TS_WIN         0x0030   /* Configure window for timestamped TX, 0: off (use normal FIFO), >0: size in ms, max depends on hardware ((2^32 - 1) ticks) */
#define NTCAN_IOCTL_GET_TX_TS_WIN         0x0031   /* Get window for timestamped TX in ms */
#define NTCAN_IOCTL_SET_TX_TS_TIMEOUT     0x0032   /* Set frame timeout for timestamped TX, 0: no timmeout, >0: timeout in ms */
#define NTCAN_IOCTL_GET_TX_TS_TIMEOUT     0x0033   /* Get frame timeout for timestamped TX */

/*
 *    Types for canFormatError()
 */
#define NTCAN_ERROR_FORMAT_LONG           0x0000   /* Error text as string        */
#define NTCAN_ERROR_FORMAT_SHORT          0x0001   /* Error code as string        */
/*
 *    Flags for canFormatEvent() (flags in NTCAN_FORMATEVENT_PARAMS)
 */
#define NTCAN_FORMATEVENT_SHORT           0x0001   /* Create a shorter description   */
/* NTCAN_FORMATEVENT_SHORT may be used to determine whether canFormatEvent is available */

/*
 * Error injection defines
 */
/* status in NTCAN_EEI_STATUS */
#define EEI_STATUS_OFF              0x0
#define EEI_STATUS_WAIT_TRIGGER     0x1
#define EEI_STATUS_SENDING          0x2
#define EEI_STATUS_FINISHED         0x3

/* mode_trigger in NTCAN_EEI_UNIT */
#define EEI_TRIGGER_MATCH           0
#define EEI_TRIGGER_ARBITRATION     1
#define EEI_TRIGGER_TIMESTAMP       2
#define EEI_TRIGGER_FIELD_POSITION  3
#define EEI_TRIGGER_EXTERNAL_INPUT  4

/* mode_trigger_option in NTCAN_EEI_UNIT */
#define EEI_TRIGGER_ARBITRATION_OPTION_ABORT_ON_ERROR   1 /* ARBITRATION MODE ONLY               */
#define EEI_TRIGGER_MATCH_OPTION_DESTUFFED              1 /* MATCH MODE ONLY                     */
#define EEI_TRIGGER_TIMESTAMP_OPTION_BUSFREE            1 /* TIMESTAMP MODE ONLY                 */

/* mode_triggerarm_delay and mode_triggeraction_delay in NTCAN_EEI_UNIT */
#define EEI_TRIGGERDELAY_NONE       0 /* no delay */
#define EEI_TRIGGERDELAY_BITTIMES   1 /* delay specified in bittimes at selected baudrate */

/*
 * Error injection structures
 */
typedef union {
        uint8_t  c[20];
        uint16_t s[10];
        uint32_t l[5];
} CAN_FRAME_STREAM;

typedef struct _NTCAN_EEI_UNIT {
        uint32_t  handle;                       /* Handle for ErrorInjection Unit       */

        uint8_t   mode_trigger;                 /* Trigger mode                         */
        uint8_t   mode_trigger_option;          /* Options to trigger                   */
        uint8_t   mode_triggerarm_delay;        /* Enable delayed arming of trigger unit*/
        uint8_t   mode_triggeraction_delay;     /* Enable delayed TX out                */
        uint8_t   mode_repeat;                  /* Enable repeat                        */
        uint8_t   mode_trigger_now;             /* Trigger with next TX point           */
        uint8_t   mode_ext_trigger_option;      /* Switch between trigger and sending   */
        uint8_t   mode_send_async;              /* Send without timing synchronization  */
        uint8_t   reserved1[4];

        uint64_t  timestamp_send;               /* Timestamp for Trigger Timestamp      */
        CAN_FRAME_STREAM trigger_pattern;       /* Trigger for mode Pattern Match       */
        CAN_FRAME_STREAM trigger_mask;          /* Mask to trigger Pattern              */
        uint8_t   trigger_ecc;                  /* ECC for Trigger Field Position       */
        uint8_t   reserved2[3];
        uint32_t  external_trigger_mask;        /* Enable Mask for external Trigger     */
        uint32_t  reserved3[16];

        CAN_FRAME_STREAM tx_pattern;            /* TX pattern                           */
        uint32_t  tx_pattern_len;               /* Length of TX pattern                 */
        uint32_t  triggerarm_delay;             /* Delay for mode triggerarm delay      */
        uint32_t  triggeraction_delay;          /* Delay for mode trigger delay         */
        uint32_t  reserved4[16];
} NTCAN_EEI_UNIT;

typedef struct _NTCAN_EEI_STATUS {
        uint32_t  handle;                       /* Handle for ErrorInjection Unit       */

        uint8_t   status;                       /* Status form Unit                     */
        uint8_t   unit_index;                   /* Error Injection Unit ID              */
        uint8_t   units_total;                  /* Max Error Units in esdacc core       */
        uint8_t   units_free;                   /* Free Error Units in esdacc core      */
        uint32_t  reserved[30];
} NTCAN_EEI_STATUS;

typedef struct _CMSG_FRAME {
        CAN_FRAME_STREAM can_frame;             /* Complete Can Frame             */
        CAN_FRAME_STREAM stuff_bits;            /* Mask of Stuff bits             */
        uint16_t  crc;                          /* CRC of CAN Frame               */
        uint8_t   length;                       /* Length of Can Frame in Bit     */

        uint8_t   pos_id11;                     /* Position of Identifier 11 Bit  */
        uint8_t   pos_id18;                     /* Position of Identifier 18 Bit  */
        uint8_t   pos_rtr;                      /* Position of RTR Bit            */
        uint8_t   pos_crtl;                     /* Position of Control Field      */
        uint8_t   pos_dlc;                      /* Position of DLC Bits           */
        uint8_t   pos_data[8];                  /* Position of Data Field         */
        uint8_t   pos_crc;                      /* Position of CRC Field          */
        uint8_t   pos_crc_del;                  /* Position of CRC delimiter      */
        uint8_t   pos_ack;                      /* Position of ACK Field          */
        uint8_t   pos_eof;                      /* Position of End of Frame       */
        uint8_t   pos_ifs;                      /* Position of Inter Frame Space  */
        uint8_t   reserved[3];
} CMSG_FRAME;


/*
 *                    NTCAN-API exported by the library
 *
 * Some parameter are passed as a pointer to a value where the application has
 * to initialize this parameter before calling the function and the parameter
 * may be changed after return. These parameters are described with IN and OUT
 * seen from the application point of view. The OUT value has to be initialized
 * by the application before the function is called, the IN value can be checked
 * by the application after return of this call.
 *
 */
EXPORT NTCAN_RESULT CALLTYPE canSetBaudrate(
                NTCAN_HANDLE   handle,      /* Handle                        */
                uint32_t       baud );      /* Baudrate                      */
/*...........................................................................*/
EXPORT NTCAN_RESULT CALLTYPE canGetBaudrate(
                NTCAN_HANDLE   handle,      /* Handle                        */
                uint32_t       *baud );     /* OUT: N/A                      */
                                            /* IN:  Baudrate                 */
/*...........................................................................*/
EXPORT NTCAN_RESULT CALLTYPE canOpen(
                int          net,          /* net number                     */
                uint32_t     flags,        /* mode flags                     */
                int32_t      txqueuesize,  /* nr of entries in message queue */
                int32_t      rxqueuesize,  /* nr of entries in message queue */
                int32_t      txtimeout,    /* tx-timeout in milliseconds     */
                int32_t      rxtimeout,    /* rx-timeout in milliseconds     */
                NTCAN_HANDLE *handle );    /* OUT: N/A                       */
                                           /* IN: Handle                     */
/*...........................................................................*/
EXPORT NTCAN_RESULT CALLTYPE canClose(
                NTCAN_HANDLE  handle );  /* Handle                           */
/*...........................................................................*/
EXPORT NTCAN_RESULT CALLTYPE canIdAdd(
                NTCAN_HANDLE  handle,    /* Handle                           */
                int32_t       id  );     /* CAN message identifier           */
/*...........................................................................*/
EXPORT NTCAN_RESULT CALLTYPE canIdRegionAdd(
                NTCAN_HANDLE  handle,    /* Handle                           */
                int32_t       idStart,   /* 1st CAN message identifier       */
                int32_t       *idCnt );  /* Last CAN message identifier      */
/*...........................................................................*/
EXPORT NTCAN_RESULT CALLTYPE canIdDelete(
                NTCAN_HANDLE  handle,    /* Handle                           */
                int32_t       id  );     /* CAN message identifier           */
/*...........................................................................*/
EXPORT NTCAN_RESULT CALLTYPE canIdRegionDelete(
                NTCAN_HANDLE  handle,    /* Handle                           */
                int32_t       idStart,   /* 1st CAN message identifier       */
                int32_t       *idCnt );  /* Last CAN message identifier      */
/*...........................................................................*/
EXPORT NTCAN_RESULT CALLTYPE canGetOverlappedResult(
                NTCAN_HANDLE  handle,    /* Handle                           */
                OVERLAPPED    *ovrlppd,  /* OUT: Win32 overlapped structure  */
                                         /* IN:  N/A                         */
                int32_t       *len,      /* OUT: N/A                         */
                                         /* IN:  # of available CMSG-Buffer  */
                BOOL          bWait );   /* FALSE =>do not wait, else wait   */
/*...........................................................................*/
EXPORT NTCAN_RESULT CALLTYPE canTake(
                NTCAN_HANDLE  handle,    /* Handle                           */
                CMSG          *cmsg,     /* Ptr to application buffer        */
                int32_t       *len );    /* OUT: Size of CMSG-Buffer         */
                                         /* IN:  Number of received messages */
/*...........................................................................*/
EXPORT NTCAN_RESULT CALLTYPE canRead(
                NTCAN_HANDLE  handle,    /* Handle                           */
                CMSG          *cmsg,     /* Ptr to application buffer        */
                int32_t       *len,      /* OUT: Size of CMSG-Buffer         */
                                         /* IN:  Number of received messages */
                OVERLAPPED    *ovrlppd); /* NULL or overlapped-structure     */
/*...........................................................................*/
EXPORT NTCAN_RESULT CALLTYPE canSend(
                NTCAN_HANDLE  handle,    /* Handle                           */
                CMSG          *cmsg,     /* Ptr to application buffer        */
                int32_t       *len );    /* OUT: # of messages to transmit   */
                                         /* IN:  # of transmitted messages   */
/*...........................................................................*/
EXPORT NTCAN_RESULT CALLTYPE canWrite(
                NTCAN_HANDLE  handle,    /* Handle                           */
                CMSG          *cmsg,     /* Ptr to application buffer        */
                int32_t       *len,      /* OUT: # of messages to transmit   */
                                         /* IN:  # of transmitted messages   */
                OVERLAPPED    *ovrlppd); /* NULL or overlapped structure     */
/*...........................................................................*/
_NTCAN_OBSOLETE(canRead) EXPORT NTCAN_RESULT CALLTYPE canReadEvent(
                NTCAN_HANDLE  handle,    /* Handle                           */
                EVMSG         *evmsg,    /* ptr to event-msg-buffer          */
                OVERLAPPED    *ovrlppd); /* NULL or overlapped structure     */
/*...........................................................................*/
_NTCAN_OBSOLETE(canSend) EXPORT NTCAN_RESULT CALLTYPE canSendEvent(
                NTCAN_HANDLE  handle,    /* Handle                           */
                EVMSG         *evmsg );  /* ptr to event-msg-buffer          */
/*...........................................................................*/
EXPORT NTCAN_RESULT CALLTYPE canStatus(
                NTCAN_HANDLE  handle,    /* Handle                           */
                CAN_IF_STATUS *cstat );  /* Ptr to can-status-buffer         */
/*...........................................................................*/
EXPORT NTCAN_RESULT CALLTYPE canIoctl(
                NTCAN_HANDLE  handle,    /* Handle                           */
                uint32_t      ulCmd,     /* Command specifier                */
                void *        pArg );    /* Ptr to command specific argument */
/*...........................................................................*/
EXPORT NTCAN_RESULT CALLTYPE canTakeT(
                NTCAN_HANDLE  handle,    /* Handle                           */
                CMSG_T        *cmsg_t,   /* Ptr to application buffer        */
                int32_t       *len );    /* OUT: Size of CMSG_T-Buffer       */
                                         /* IN:  Number of received messages */
/*...........................................................................*/
EXPORT NTCAN_RESULT CALLTYPE canReadT(
                NTCAN_HANDLE  handle,    /* Handle                           */
                CMSG_T        *cmsg_t,   /* Ptr to application buffer        */
                int32_t       *len,      /* OUT: Size of CMSG_T-Buffer       */
                                         /* IN:  Number of received messages */
                OVERLAPPED    *ovrlppd); /* In: always NULL                  */
/*...........................................................................*/
EXPORT NTCAN_RESULT CALLTYPE canSendT(
                NTCAN_HANDLE  handle,    /* Handle                           */
                CMSG_T        *cmsg_t,   /* Ptr to application buffer        */
                int32_t       *len );    /* OUT: # of messages to transmit   */
                                         /* IN:  # of transmitted messages   */
/*...........................................................................*/
EXPORT NTCAN_RESULT CALLTYPE canWriteT(
                NTCAN_HANDLE  handle,    /* handle                           */
                CMSG_T        *cmsg_t,   /* ptr to data-buffer               */
                int32_t       *len,      /* OUT: # of messages to transmit   */
                                         /* IN:  # of transmitted messages   */
                OVERLAPPED    *ovrlppd); /* NULL or overlapped structure     */
/*...........................................................................*/
EXPORT NTCAN_RESULT CALLTYPE canFormatError(
                NTCAN_RESULT  error,     /* Error code                       */
                uint32_t      type,      /* Error message type               */
                char          *pBuf,     /* Pointer to destination buffer    */
                uint32_t      bufsize);  /* Size of the buffer above         */
/*...........................................................................*/
EXPORT NTCAN_RESULT CALLTYPE canFormatEvent(
                EVMSG         *event,    /* Event message                    */
                NTCAN_FORMATEVENT_PARAMS *para, /* Parameters                */
                char          *pBuf,     /* Pointer to destination buffer    */
                uint32_t      bufsize);  /* Size of the buffer above         */
/*...........................................................................*/
EXPORT NTCAN_RESULT CALLTYPE canGetOverlappedResultT(
                NTCAN_HANDLE  handle,    /* Handle                           */
                OVERLAPPED    *ovrlppd,  /* OUT: Win32 overlapped structure  */
                                         /* IN:  N/A                         */
                int32_t       *len,      /* OUT: N/A                         */
                                         /* IN:  # of available CMSG_T-Buffer*/
                BOOL          bWait );   /* FALSE =>do not wait, else wait   */
/*...........................................................................*/
EXPORT NTCAN_RESULT CALLTYPE canFormatFrame(
                CMSG          *msg,      /* CAN message                      */
                CMSG_FRAME    *frame,    /* CAN Frame + Information          */
                uint32_t      eccExt);   /* ECC Errors + Features            */


#if !defined(UNDER_RTSS) && !defined(RTOS32)

/*
 * Function pointer to ease loading DLL dynamically
 */
typedef NTCAN_RESULT (CALLTYPE *PFN_CAN_SET_BAUDRATE)(NTCAN_HANDLE handle,
                                                      uint32_t     baud);
typedef NTCAN_RESULT (CALLTYPE *PFN_CAN_GET_BAUDRATE)(NTCAN_HANDLE handle,
                                                      uint32_t     *baud);
typedef NTCAN_RESULT (CALLTYPE *PFN_CAN_OPEN)(int          net,
                                              uint32_t     flags,
                                              int32_t      txqueuesize,
                                              int32_t      rxqueuesize,
                                              int32_t      txtimeout,
                                              int32_t      rxtimeout,
                                              NTCAN_HANDLE *handle );
typedef NTCAN_RESULT (CALLTYPE *PFN_CAN_CLOSE)(NTCAN_HANDLE handle);
typedef NTCAN_RESULT (CALLTYPE *PFN_CAN_ID_ADD)(NTCAN_HANDLE handle,
                                                int32_t      id);
typedef NTCAN_RESULT (CALLTYPE *PFN_CAN_ID_DELETE)(NTCAN_HANDLE handle,
                                                   int32_t id);
typedef NTCAN_RESULT (CALLTYPE *PFN_CAN_GET_OVERLAPPED_RESULT)(NTCAN_HANDLE handle,
                                                               OVERLAPPED   *ovrlppd,
                                                               int32_t      *len,
                                                               BOOL         bWait);
typedef NTCAN_RESULT (CALLTYPE *PFN_CAN_SEND)(NTCAN_HANDLE handle,
                                              CMSG         *cmsg,
                                              int32_t      *len);
typedef NTCAN_RESULT (CALLTYPE *PFN_CAN_WRITE)(NTCAN_HANDLE handle,
                                               CMSG         *cmsg,
                                               int32_t      *len,
                                               OVERLAPPED   *ovrlppd);
typedef NTCAN_RESULT (CALLTYPE *PFN_CAN_TAKE)(NTCAN_HANDLE handle,
                                              CMSG         *cmsg,
                                              int32_t      *len);
typedef NTCAN_RESULT (CALLTYPE *PFN_CAN_READ)(NTCAN_HANDLE handle,
                                              CMSG         *cmsg,
                                              int32_t      *len,
                                              OVERLAPPED   *ovrlppd);
typedef NTCAN_RESULT (CALLTYPE *PFN_CAN_READ_EVENT)(NTCAN_HANDLE handle,
                                                    EVMSG        *cmsg,
                                                    OVERLAPPED   *ovrlppd);
typedef NTCAN_RESULT (CALLTYPE *PFN_CAN_SEND_EVENT)(NTCAN_HANDLE handle,
                                                    EVMSG        *cmsg);
typedef NTCAN_RESULT (CALLTYPE *PFN_CAN_STATUS)(NTCAN_HANDLE  handle,
                                                CAN_IF_STATUS *cstat);
typedef NTCAN_RESULT (CALLTYPE *PFN_CAN_IOCTL)(NTCAN_HANDLE handle,
                                               uint32_t     ulCmd,
                                               void         *pArg );
typedef NTCAN_RESULT (CALLTYPE *PFN_CAN_TAKE_T)(NTCAN_HANDLE handle,
                                                CMSG_T       *cmsg_t,
                                                int32_t      *len);
typedef NTCAN_RESULT (CALLTYPE *PFN_CAN_READ_T)(NTCAN_HANDLE handle,
                                                CMSG_T       *cmsg_t,
                                                int32_t      *len,
                                                OVERLAPPED   *ovrlppd);
typedef NTCAN_RESULT (CALLTYPE *PFN_CAN_SEND_T)(NTCAN_HANDLE handle,
                                                CMSG_T       *cmsg_t,
                                                int32_t      *len);
typedef NTCAN_RESULT (CALLTYPE *PFN_CAN_WRITE_T)(NTCAN_HANDLE handle,
                                                 CMSG_T       *cmsg_t,
                                                 int32_t      *len,
                                                 OVERLAPPED   *ovrlppd);
typedef NTCAN_RESULT (CALLTYPE *PFN_CAN_FORMAT_ERROR)(NTCAN_RESULT  error,
                                                      uint32_t      type,
                                                      char          *pBuf,
                                                      uint32_t      bufsize);
typedef NTCAN_RESULT (CALLTYPE *PFN_CAN_FORMAT_EVENT)(EVMSG         *event,
                                                      NTCAN_FORMATEVENT_PARAMS *para,
                                                      char          *pBuf,
                                                      uint32_t      bufsize);
typedef NTCAN_RESULT (CALLTYPE *PFN_CAN_GET_OVERLAPPED_RESULT_T)(NTCAN_HANDLE handle,
                                                                 OVERLAPPED   *ovrlppd,
                                                                 int32_t      *len,
                                                                 BOOL         bWait);
typedef NTCAN_RESULT (CALLTYPE *PFN_CAN_FORMAT_FRAME)(CMSG        *msg,
                                                      CMSG_FRAME  *frame,
                                                      uint32_t    eccExt);

/*
 * Macros to ease loading DLL dynamically
 */
#define FUNCPTR_CAN_SET_BAUDRATE(hDll) \
(PFN_CAN_SET_BAUDRATE)GetProcAddress(hDll, "canSetBaudrate")
#define FUNCPTR_CAN_GET_BAUDRATE(hDll) \
(PFN_CAN_GET_BAUDRATE)GetProcAddress(hDll, "canGetBaudrate")
#define FUNCPTR_CAN_OPEN(hDll) \
(PFN_CAN_OPEN)GetProcAddress(hDll, "canOpen")
#define FUNCPTR_CAN_CLOSE(hDll) \
(PFN_CAN_CLOSE)GetProcAddress(hDll, "canClose")
#define FUNCPTR_CAN_ID_ADD(hDll) \
(PFN_CAN_ID_ADD)GetProcAddress(hDll, "canIdAdd")
#define FUNCPTR_CAN_ID_DELETE(hDll) \
(PFN_CAN_ID_DELETE)GetProcAddress(hDll, "canIdDelete")
#define FUNCPTR_CAN_GET_OVERLAPPED_RESULT(hDll) \
(PFN_CAN_GET_OVERLAPPED_RESULT)GetProcAddress(hDll, "canGetOverlappedResult")
#define FUNCPTR_CAN_SEND(hDll) \
(PFN_CAN_SEND)GetProcAddress(hDll, "canSend")
#define FUNCPTR_CAN_WRITE(hDll) \
(PFN_CAN_WRITE)GetProcAddress(hDll, "canWrite")
#define FUNCPTR_CAN_TAKE(hDll) \
(PFN_CAN_TAKE)GetProcAddress(hDll, "canTake")
#define FUNCPTR_CAN_READ(hDll) \
(PFN_CAN_READ)GetProcAddress(hDll, "canRead")
#define FUNCPTR_CAN_SEND_EVENT(hDll) \
(PFN_CAN_SEND_EVENT)GetProcAddress(hDll, "canSendEvent")
#define FUNCPTR_CAN_READ_EVENT(hDll) \
(PFN_CAN_READ_EVENT)GetProcAddress(hDll, "canReadEvent")
#define FUNCPTR_CAN_STATUS(hDll) \
(PFN_CAN_STATUS)GetProcAddress(hDll, "canStatus")
#define FUNCPTR_CAN_IOCTL(hDll) \
(PFN_CAN_IOCTL)GetProcAddress(hDll, "canIoctl")
#define FUNCPTR_CAN_SEND_T(hDll) \
(PFN_CAN_SEND_T)GetProcAddress(hDll, "canSendT")
#define FUNCPTR_CAN_WRITE_T(hDll) \
(PFN_CAN_WRITE_T)GetProcAddress(hDll, "canWriteT")
#define FUNCPTR_CAN_TAKE_T(hDll) \
(PFN_CAN_TAKE_T)GetProcAddress(hDll, "canTakeT")
#define FUNCPTR_CAN_READ_T(hDll) \
(PFN_CAN_READ_T)GetProcAddress(hDll, "canReadT")
#define FUNCPTR_CAN_FORMAT_ERROR(hDll) \
(PFN_CAN_FORMAT_ERROR)GetProcAddress(hDll, "canFormatError")
#define FUNCPTR_CAN_FORMAT_EVENT(hDll) \
(PFN_CAN_FORMAT_EVENT)GetProcAddress(hDll, "canFormatEvent")
#define FUNCPTR_CAN_GET_OVERLAPPED_RESULT_T(hDll) \
(PFN_CAN_GET_OVERLAPPED_RESULT_T)GetProcAddress(hDll, "canGetOverlappedResultT")
#define FUNCPTR_CAN_FORMAT_FRAME(hDll) \
(PFN_CAN_FORMAT_FRAME)GetProcAddress(hDll, "canFormatFrame")
#endif /* of !defined UNDER_RTSS */

#ifdef __cplusplus
}
#endif

#endif /* _ntcan_h_ */

