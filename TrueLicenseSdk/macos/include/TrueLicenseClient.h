#pragma once
#include "Constants.h"
#include "TrueLicenseTypes.h"
#include <vector>
#include <memory>

// Forward declarations
namespace web
{
  namespace json
  {
    class value;
  }
}

namespace TrueLicenseSdk
{
  // Forward declarations
  namespace Common
  {
    class TlSdkHttpClient;
  }

  namespace Handlers
  {
    class LicenseFileHandler;
    class Cryptography;
    class HardwareKeyGenerator;
    class DateTime;
  }

	// library version
	static const char* VERSION = "1.1.0";

	/* license types enum
	*/
	enum class LicenseType {
		Trial = 0,
		Unlimited = 1
	};

	enum class ErrorCode
	{
		None = 0, /**< no error  */
		NetworkError = 1, /**< could not contact the server */
		LicenseFileError = 2, /**< could not I/O the license file / is missing filds / is corrupted */
		NotFoundServerError = 3, /**< server returns error Not Found(404) usually occurs when the productId is invalid or the user is already registered (please check the documentation for more details) */
		InvalidInputError = 4, /**< server returns BadRequest(400) sometimes with a message */
		HardwareKeyGenerationError = 5,/**< could not generate the hardware key */
		AlreadyLicensedError = 6, /**< client returns this error if the product is already licensed and any register method is called  */
		NotLicensedError = 7, /**< client returns this error if the product is not licensed and any update method is called  */
		Other = 8 /**< other errors */
	};

	/** Client Result struct that will be returned by every TrueLicenseClient method that makes a request to the server
	*/
	struct ClientResult
	{
		/* By default the result is a success
		*/
		ClientResult() :
			mSuccess(true),
			mErrorCode(ErrorCode::None),
			mMessage(STRING(""))
		{}

		void setError(ErrorCode _errorCode, Types::string _message)
		{
			mSuccess = false;
			mErrorCode = _errorCode;
			mMessage = _message;
		}

		bool mSuccess; /**< True if all goes well otherwise False */
		ErrorCode mErrorCode; /**<  error code, None for a success result */
		Types::string mMessage; /**<  error message, empty on success */
	};

	/** LicenseFileBoolResult struct that will be returned by every TrueLicenseClient method that reads the license file for different purposes
	*/
	struct LicenseFileBoolResult
	{
		/* By default the result is a success
		*/
		LicenseFileBoolResult() :
			mValue(true),
			mErrorCode(ErrorCode::None)
		{}

		void setError(ErrorCode _errorCode)
		{
			mValue = false;
			mErrorCode = _errorCode;
		}

		bool mValue; /**< True if all goes well otherwise False */
		ErrorCode mErrorCode; /**<  error code, None for a success result */
	};

	/** Client info for server url and license file
	*/
	struct ClientInfo
	{
		Types::string mServerUrl; /**<  server url */
		Types::string mLicenseFilePath; /**<  license file path */
	};

	/** Product info for a hardware dependent product
	*/
	struct HardwareDependentProductInfo
	{
		Types::string mProductId; /**< product id ,can be found on TrueLicense server, generated on product creation*/
		Types::string mProductVersion; /**< current product version , the server response will contains the licenses available for this version */
	};

	/** User information for an hardware dependent, this should match the values from the TrueLicense Server
	*/
	struct HardwareDependentUserInfo
	{
		Types::string mSerial; /**<  (Required)product serial used to register a hardware dependent product */
		Types::string mUsername; /**<  (Required)User name */
		Types::string mEmail; /**<(Required) user email */
		Types::string mAffiliation; /**< (Optional) user affiliation  */
		Types::string mOther; /**< (Optional) user other input */
	};

	/** Product info for a hardware independent product
	*/
	struct HardwareIndependentProductInfo
	{
		Types::string mProductId; /**< product id ,can be found on TrueLicense server, generated on product creation*/
	};

	/** User information for an hardware independent
	*/
	struct HardwareIndependentUserInfo
	{
		Types::string mUsername; /**< (Required) username */
		Types::string mEmail; /*< (Required) email */
	};

	/** Activation information for an hardware independent
	*/
	struct HardwareIndependentActivationInfo
	{
		Types::string mActivationKey; /*< (Required) activation key */
	};


	/** license infos class
	*   holds infos about a license : licenseId,status,type,remaining days
	*/
	class License
	{
	public:
		/** Initialize a new empty license to be passed by reference
		*/
		License(){}

		/** Initialize a new license for a given id,type,expirationDate
		* @param _licenseId[in] license id
		* @param _licenseType[in] license type
		* @param _expirationDate[in] expiration date
		*
		*/
		License(const Types::string& _licenseId,
			LicenseType _licenseType,
			const Types::string& _expirationDate)
			:
			m_licenseId(_licenseId),
			m_licenseType(_licenseType)
		{
			// in some cases server sets the license expiration date to 0001-01-01T00:00:00 (DateTimeConstants::serverMinDate)
			// boost fails to parse this date and throws exception
			if (_expirationDate.compare(DateTimeConstants::serverMinDate) == 0)
			{
				m_expirationDate = DateTimeConstants::minDateNormalFormat;
			}
			else
			{
				m_expirationDate =_expirationDate;
			}
		}
		/// destructor
		~License();

		/** Gets the license id
		*/
		Types::string getLicenseId();

		/** Gets the license status based on expiration and system date
		*/
		bool isExpired();

		/** Gets the license type
		*/
		LicenseType getLicenseType();

		/** Gets the remaining days until expiration (note that 0 days does not mean that the license already expired,use the isExpired method for that)
		*/
		int remainingDays();
	private:
		Types::string m_licenseId; /**< license id */
		LicenseType m_licenseType; /**< license type */
		Types::string m_expirationDate; /**< expiration date */
	};

	/** Defines TrueLicense Client to register and manage license details of a product
	*
	*   Client performs blocking HTTP calls using the Microsoft "cpprestsdk" library
	*   and therefore is meant to be run from a non-critical thread.
	*
	*   Client handles all exceptions related to network and file I/O.
	*
	*/
	class TrueLicenseClient
	{
	public:
		/** Initialize a new client
		* @param _clientInfo[in] struct that hold server url and license file path
		* @param _logger[in] logger pointer function
		*/
    TrueLicenseClient(const ClientInfo& _clientInfo,
                      void(*_logger)(const char*));

		/** Constructor for mock
		*/
		TrueLicenseClient(const ClientInfo& _clientInfo,
			void(*_logger)(const char*),
			std::shared_ptr<Common::TlSdkHttpClient> _httpClient,
			std::shared_ptr<Handlers::LicenseFileHandler> _licenseFileHandler,
			std::shared_ptr<Handlers::Cryptography> _cryptography,
			std::shared_ptr<Handlers::HardwareKeyGenerator> _hardwareKeyGenerator,
			std::shared_ptr<Handlers::DateTime> _dateTimeHandler)
			:
			mLogger(_logger),
			mClient(_httpClient),
			mLicenseFileHandler(_licenseFileHandler),
			mCrypto(_cryptography),
			mHardwareKeyGenerator(_hardwareKeyGenerator),
			mDateTimeHandler(_dateTimeHandler)
		{
		}

		/** destructor.
		*/
		~TrueLicenseClient()
		{
			mLicenseFileHandler.reset();
			mCrypto.reset();
			mHardwareKeyGenerator.reset();
			mClient.reset();
			mDateTimeHandler.reset();
		}

		/** registers an user for a hardware on the TrueLicense Server based on productInfo and userInfo(the values must match the ones from the TrueLicense Server)
		*	and process a response by creating the license file
		*
		* @param _productInfo[in] properties are required
		* @param _userInfo[in] user info ,in order to get a success result , Serial,Username,Email are Required and the entire struct properties must match the User Info values from TrueLicense Server
		*/
		ClientResult registerHardwareDependent(const HardwareDependentProductInfo& _productInfo, const HardwareDependentUserInfo& _userInfo);

		/** Updates the license file  or run the offline validation
		* @param _productInfo[in] properties are required
		*/
		ClientResult updateHardwareDependent(const HardwareDependentProductInfo& _productInfo);

		/** registers an user on the TrueLicense Server based on productInfo and userInfo(the values must match the ones from the TrueLicense Server,username can be different)
		*
		* @param _productInfo[in] properties are required
		* @param _userInfo[in] user info ,in order to get a success result , Serial,Username,Email are Required and the entire struct properties must match the User Info values from TrueLicense Server
		*/
		ClientResult registerHardwareIndependent(const HardwareIndependentProductInfo& _productInfo, const HardwareIndependentUserInfo& _userInfo);

		/** activates an user on the TrueLicense Server based on productInfo and userInfo(the values must match the ones from the TrueLicense Server,username can be different)
		*	and process a response by creating the license file
		*
		* @param _productInfo[in] properties are required
		* @param _activationInfo[in] user info ,in order to get a success result , activation key must match the one from the email sent by the server
		*/
		ClientResult activateHardwareIndependent(const HardwareIndependentProductInfo& _productInfo, const HardwareIndependentActivationInfo& _activationInfo);

		/** Updates the license file  or run the offline validation
		* @param _productInfo[in] properties are required
		*/
		ClientResult updateHardwareIndependent(const HardwareIndependentProductInfo& _productInfo);

		/** Checks if the current product is licensed.
		Returns True if license file exists,False otherwise.
		*/
		LicenseFileBoolResult isProductLicensed();

		/** Checks if the license file had been updated by the server in a number of given days (by default 14)
		* @param _days[in] number of days
		* Returns True if the server update happend in the last [_days] days or False otherwise.
		*/
		LicenseFileBoolResult hadServerUpdateIn(const int& _days = 14);


		/** populates the vector passed as argument
		@param _resultLicenses[out] the licenses
		Return True if the vector was populated otherwise false
		*/
		LicenseFileBoolResult getHardwareDependentLicenses(std::vector<License>& _resultLicenses);

		/** updates the license passed as argument
		@param _resultLicense[out] the license
		Return True if the reference was updated otherwise false
		*/
		LicenseFileBoolResult getHardwareIndependentLicense(License& _resultLicense);

	private:
		/** loads the values used to build the update request and runs license file checks
		@param _licenseFileJson[out] license file as json
		@param _hardwareKey[out] hardware key
		Returns client result with mSuccess true if all values are initialized otherwise false
		*/
		ClientResult initializeUpdateValues(web::json::value& _licenseFileJson, Types::string& _hardwareKey);

		/** initialize the hardware key and runs license file checks
		@param _hardwareKey[out] hardware key
		Returns client result with mSuccess true if the hardware key is initialized otherwise false
		*/
		ClientResult licensingChecks(Types::string& _hardwareKey);

		/** runs the offline validation
		@param _licenseJson[in] license file json
		@param _hardwareKey[in] hardware key
		Returns result with True success and None error if all goes well
		*/
		ClientResult offlineValidation(web::json::value _licenseFileJson,Types::string _hardwareKey);

		/** creates a new license file
		@param _serverLicenseJson[in] json received from the server
		Returns result with True success and None error if all goes well
		*/
		ClientResult createLicenseFile(web::json::value _serverLicenseJson);

		/** checks if a license json has all required fields
		@param _licenseAsJson[in] license values in json format
		Returns True if the json contains all required fields otherwise False;
		*/
		bool licenseJsonHasAllFields(web::json::value _licenseAsJson);

		/** validates certificates for all license json values
		@param _licenseAsJson[in] license values in json format
		@param _key[in] public key used to check validity
		Returns True if certificates ar valid otherwise False
		*/
		bool areLicenseJsonCertificatesValid(web::json::value _licenseAsJson, Types::string _key);

		/** Creates a new License from the json format
		@param _licenseAsJson[in] license values in json format
		@param _isLastSystemDateValid[in] last system date validity in order to prevent datetime hacking
		Returns License
		*/
		License createLicenseFromJson(web::json::value _licenseAsJson, bool _isLastSystemDateValid);
		

		void(*mLogger)(const char*); /**< the logger function pointer */
		std::shared_ptr<Handlers::LicenseFileHandler> mLicenseFileHandler; /**< license file handler */
		std::shared_ptr<Handlers::Cryptography> mCrypto; /**< cryptography handler */
		std::shared_ptr<Handlers::HardwareKeyGenerator> mHardwareKeyGenerator; /**< hardware key generator */
		std::shared_ptr<Common::TlSdkHttpClient> mClient; /**< cppsdk client */
		std::shared_ptr<Handlers::DateTime> mDateTimeHandler; /**< date time computing handler */
	};

}