#pragma once
#include "TrueLicenseTypes.h"

namespace TrueLicenseSdk
{
	using namespace std;
	namespace Handlers
	{
		class HardwareKeyGenerator
		{
		public:
			HardwareKeyGenerator() {};
			virtual Types::string getHardwareKey();
		};
	}
}