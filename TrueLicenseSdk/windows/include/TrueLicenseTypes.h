#pragma once
#include <string>

namespace TrueLicenseSdk
{
	namespace Types
	{
#ifdef _WIN32

		using string = std::wstring;
#define GLUE(x,y) x##y
#define STRING(x) GLUE(L,x)

#elif __APPLE__
#define STRING(x) x
		using string = std::string;

#endif
	}
}