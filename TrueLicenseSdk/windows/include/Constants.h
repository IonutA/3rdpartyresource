#pragma once
#include "TrueLicenseTypes.h"

namespace TrueLicenseSdk
{

	namespace ClientResultMessages
	{
		static const Types::string checkingLicenseFileExistence = STRING("Cannot check if license file exists");
		static const Types::string missingLicenseFileFields = STRING("Missing license file fields");
		static const Types::string alreadyLicensed = STRING("The product is already licensed");
		static const Types::string notLicensed = STRING("The product is not licensed");
		static const Types::string hardwareKeyGeneration = STRING("Hardware Key generation failed");
		static const Types::string invalidUserInformation = STRING("Invalid user information");
		static const Types::string invalidRequest = STRING("Server failed to process the request");
		static const Types::string creatingLicenseFileError = STRING("Error occured while creating the license file");
		static const Types::string readingLicenseFileError = STRING("Error occured while reading the license file");
		static const Types::string updatingLicenseFileError = STRING("Error occured while updating the license file");
		static const Types::string contactingTheServerError = STRING("There was an error contacting the server.Please check your network connection and try again.");
		static const Types::string otherError = STRING("Error occured");
		static const Types::string invalidHardwareKeyCertificate = STRING("Invalid hardware key certificate");
	}

	namespace HardwareToolMessages
	{
		static const Types::string invalidCredentials = STRING("Incorrect username or password");
		static const Types::string loginFailed = STRING("Login failed");
		static const Types::string deployFailed = STRING("Deploy failed");
		static const Types::string hardwareKeyGenerationFailed = STRING("Deploy failed");
		static const Types::string getProductsFailed = STRING("Get Products failed");
		static const Types::string internetConnectionRequired = STRING("Internet Connection is required");
	}

	namespace ApiConstants
	{
		static const Types::string api = STRING("api");
		static const Types::string external = STRING("external");
		static const Types::string licensetvsproduct = STRING("licensetvsproduct");
		static const Types::string checktvsproductlicenses = STRING("checktvsproductlicenses");

		static const Types::string licenseproduct = STRING("licenseproduct");
		static const Types::string validateemailedlicense = STRING("validateemailedlicense");
		static const Types::string checkproductlicense = STRING("checkproductlicense");

		static const Types::string deployhardware = STRING("deployhardware");
		static const Types::string token = STRING("token");
		static const Types::string getProducts = STRING("getproducts");

		static const Types::string granttype = STRING("grant_type");
		static const Types::string username = STRING("username");
		static const Types::string password = STRING("password");


		static const Types::string contentType = STRING("Content-Type");
		static const Types::string contentTypeUrlEncoded = STRING("application/x-www-form-urlencoded");
		static const Types::string contentTypeJson = STRING("");
		static const Types::string authorization = STRING("Authorization");
		static const Types::string bearer = STRING("Bearer ");

		static const Types::string productId = STRING("productId");
		static const Types::string email = STRING("email");

		static const Types::string hardwareKey = STRING("hardwareKey");
		static const Types::string activationKey = STRING("activationKey");

		static const Types::string invalidUserInformation = STRING("Invalid User Information");
	}

	namespace LicenseJsonConstants
	{
		static const Types::string lastSystemDate = STRING("lastSystemDate");
		static const Types::string lastSyncDate = STRING("lastSyncDate");
		static const Types::string userId = STRING("userId");
		static const Types::string licenses = STRING("licenses");
		static const Types::string key = STRING("key");
		static const Types::string hardwareKey = STRING("hardwareKey");
		static const Types::string licenseKey = STRING("licenseKey");
		static const Types::string licenseId = STRING("licenseId");
		static const Types::string licenseExpirationDate = STRING("licenseExpirationDate");
		static const Types::string licenseExpirationDateCertificate = STRING("licenseExpirationDateCertificate");
		static const Types::string licenseType = STRING("licenseType");
		static const Types::string licenseTypeCertificate = STRING("licenseTypeCertificate");
		const static Types::string trialString = STRING("Trial");
	}

	namespace ServerJsonConstants
	{
		static const Types::string productId = STRING("productId");
		static const Types::string version = STRING("version");
		static const Types::string hardwareKey = STRING("hardwareKey");
		static const Types::string serial = STRING("serial");
		static const Types::string systemDate = STRING("systemDate");
		static const Types::string username = STRING("username");
		static const Types::string email = STRING("email");
		static const Types::string affiliation = STRING("affiliation");
		static const Types::string other = STRING("other");
		static const Types::string userInformation = STRING("userInformation");
		static const Types::string userInfoId = STRING("userInfoId");
		static const Types::string licenses = STRING("licenses");
		static const Types::string id = STRING("id");
		static const Types::string name = STRING("name");
		static const Types::string products = STRING("products");

		static const Types::string licenseKey = STRING("licenseKey");
		static const Types::string licenseId = STRING("licenseId");

		static const Types::string accesstoken = STRING("access_token");
		static const Types::string responseMessage = STRING("message");

		static const Types::string valid = STRING("valid");
		static const Types::string add = STRING("add");
	}

	namespace DateTimeConstants
	{
		static const Types::string minDateNormalFormat = STRING("1900-01-01T00:00:00");
		static const Types::string serverMinDate = STRING("0001-01-01T00:00:00");
	}

	namespace EnumsConstants
	{
		static const Types::string trialLicenseType = STRING("Trial");
	}

	namespace EncodeLicenseFileConstants
	{
		static const char T = 'T';
		static const char R = 'R';
		static const char U = 'U';
		static const char E = 'E';
	}
}